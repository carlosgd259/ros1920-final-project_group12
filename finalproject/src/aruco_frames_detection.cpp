#include <ros/ros.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/Pose.h>
#include <string>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf/tf.h>

using namespace std;

bool calibration_needed=false;
float bound= 0.002;
float lengthx = 0.0;
float lengthy = 0.0;
float incrx = 0.0;
float incry = 0.0;

float c100x = 0.0;
float c101x = 0.0;
float c102x = 0.0;
float c103x = 0.0;

float c100y = 0.0;
float c101y = 0.0;
float c102y = 0.0;
float c103y = 0.0;


geometry_msgs::TransformStamped ts_aruco_100;
geometry_msgs::TransformStamped ts_aruco_101;
geometry_msgs::TransformStamped ts_aruco_102;
geometry_msgs::TransformStamped ts_aruco_103;

//Black Pawns

geometry_msgs::TransformStamped ts_aruco_201;
geometry_msgs::TransformStamped ts_aruco_202;
geometry_msgs::TransformStamped ts_aruco_203;
geometry_msgs::TransformStamped ts_aruco_204;
geometry_msgs::TransformStamped ts_aruco_205;
geometry_msgs::TransformStamped ts_aruco_206;
geometry_msgs::TransformStamped ts_aruco_207;
geometry_msgs::TransformStamped ts_aruco_208;


//White Pawns

geometry_msgs::TransformStamped ts_aruco_301;
geometry_msgs::TransformStamped ts_aruco_302;
geometry_msgs::TransformStamped ts_aruco_303;
geometry_msgs::TransformStamped ts_aruco_304;
geometry_msgs::TransformStamped ts_aruco_305;
geometry_msgs::TransformStamped ts_aruco_306;
geometry_msgs::TransformStamped ts_aruco_307;
geometry_msgs::TransformStamped ts_aruco_308;


//Black Towers

geometry_msgs::TransformStamped ts_aruco_209;
geometry_msgs::TransformStamped ts_aruco_210;

//Black Horses

geometry_msgs::TransformStamped ts_aruco_211;
geometry_msgs::TransformStamped ts_aruco_212;

//Black Knights

geometry_msgs::TransformStamped ts_aruco_213;
geometry_msgs::TransformStamped ts_aruco_214;

//Black Queen
geometry_msgs::TransformStamped ts_aruco_215;

//Black King
geometry_msgs::TransformStamped ts_aruco_216;

//White Towers

geometry_msgs::TransformStamped ts_aruco_309;
geometry_msgs::TransformStamped ts_aruco_310;

//White Horses

geometry_msgs::TransformStamped ts_aruco_311;
geometry_msgs::TransformStamped ts_aruco_312;

//White Knights

geometry_msgs::TransformStamped ts_aruco_313;
geometry_msgs::TransformStamped ts_aruco_314;

//White Queen
geometry_msgs::TransformStamped ts_aruco_315;

//White King
geometry_msgs::TransformStamped ts_aruco_316;


//Defining board centers


geometry_msgs::TransformStamped ts_A1;
geometry_msgs::TransformStamped ts_A2;
geometry_msgs::TransformStamped ts_A3;
geometry_msgs::TransformStamped ts_A4;
geometry_msgs::TransformStamped ts_A5;
geometry_msgs::TransformStamped ts_A6;
geometry_msgs::TransformStamped ts_A7;
geometry_msgs::TransformStamped ts_A8;

geometry_msgs::TransformStamped ts_B1;
geometry_msgs::TransformStamped ts_B2;
geometry_msgs::TransformStamped ts_B3;
geometry_msgs::TransformStamped ts_B4;
geometry_msgs::TransformStamped ts_B5;
geometry_msgs::TransformStamped ts_B6;
geometry_msgs::TransformStamped ts_B7;
geometry_msgs::TransformStamped ts_B8;

geometry_msgs::TransformStamped ts_C1;
geometry_msgs::TransformStamped ts_C2;
geometry_msgs::TransformStamped ts_C3;
geometry_msgs::TransformStamped ts_C4;
geometry_msgs::TransformStamped ts_C5;
geometry_msgs::TransformStamped ts_C6;
geometry_msgs::TransformStamped ts_C7;
geometry_msgs::TransformStamped ts_C8;

geometry_msgs::TransformStamped ts_D1;
geometry_msgs::TransformStamped ts_D2;
geometry_msgs::TransformStamped ts_D3;
geometry_msgs::TransformStamped ts_D4;
geometry_msgs::TransformStamped ts_D5;
geometry_msgs::TransformStamped ts_D6;
geometry_msgs::TransformStamped ts_D7;
geometry_msgs::TransformStamped ts_D8;

geometry_msgs::TransformStamped ts_E1;
geometry_msgs::TransformStamped ts_E2;
geometry_msgs::TransformStamped ts_E3;
geometry_msgs::TransformStamped ts_E4;
geometry_msgs::TransformStamped ts_E5;
geometry_msgs::TransformStamped ts_E6;
geometry_msgs::TransformStamped ts_E7;
geometry_msgs::TransformStamped ts_E8;

geometry_msgs::TransformStamped ts_F1;
geometry_msgs::TransformStamped ts_F2;
geometry_msgs::TransformStamped ts_F3;
geometry_msgs::TransformStamped ts_F4;
geometry_msgs::TransformStamped ts_F5;
geometry_msgs::TransformStamped ts_F6;
geometry_msgs::TransformStamped ts_F7;
geometry_msgs::TransformStamped ts_F8;

geometry_msgs::TransformStamped ts_G1;
geometry_msgs::TransformStamped ts_G2;
geometry_msgs::TransformStamped ts_G3;
geometry_msgs::TransformStamped ts_G4;
geometry_msgs::TransformStamped ts_G5;
geometry_msgs::TransformStamped ts_G6;
geometry_msgs::TransformStamped ts_G7;
geometry_msgs::TransformStamped ts_G8;

geometry_msgs::TransformStamped ts_H1;
geometry_msgs::TransformStamped ts_H2;
geometry_msgs::TransformStamped ts_H3;
geometry_msgs::TransformStamped ts_H4;
geometry_msgs::TransformStamped ts_H5;
geometry_msgs::TransformStamped ts_H6;
geometry_msgs::TransformStamped ts_H7;
geometry_msgs::TransformStamped ts_H8;

// CallBack function Aruco 100 Pose subscribion
void aruco100Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_100.transform.translation.x = msg.pose.position.x;
  ts_aruco_100.transform.translation.y = msg.pose.position.y;
  ts_aruco_100.transform.translation.z = msg.pose.position.z; 
  ts_aruco_100.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_100.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_100.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_100.transform.rotation.w = msg.pose.orientation.w;
  c100x =  msg.pose.position.x;
  c100y =  msg.pose.position.y;
}

// CallBack function Aruco 101 Pose subscribion
void aruco101Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_101.transform.translation.x = msg.pose.position.x;
  ts_aruco_101.transform.translation.y = msg.pose.position.y;
  ts_aruco_101.transform.translation.z = msg.pose.position.z;
  ts_aruco_101.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_101.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_101.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_101.transform.rotation.w = msg.pose.orientation.w;
  c101x =  msg.pose.position.x;
  c101y =  msg.pose.position.y;
}

// CallBack function Aruco 102 Pose subscribion
void aruco102Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_102.transform.translation.x = msg.pose.position.x;
  ts_aruco_102.transform.translation.y = msg.pose.position.y;
  ts_aruco_102.transform.translation.z = msg.pose.position.z; 
  ts_aruco_102.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_102.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_102.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_102.transform.rotation.w = msg.pose.orientation.w;
  c102x =  msg.pose.position.x;
  c102y =  msg.pose.position.y;
}

// CallBack function Aruco 103 Pose subscribion
void aruco103Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_103.transform.translation.x = msg.pose.position.x;
  ts_aruco_103.transform.translation.y = msg.pose.position.y;
  ts_aruco_103.transform.translation.z = msg.pose.position.z;
  ts_aruco_103.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_103.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_103.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_103.transform.rotation.w = msg.pose.orientation.w;
  c103x =  msg.pose.position.x;
  c103y =  msg.pose.position.y;
}



// CallBack function Aruco 201 Pose subscribion
void aruco201Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_201.transform.translation.x = msg.pose.position.x;
  ts_aruco_201.transform.translation.y = msg.pose.position.y;
  ts_aruco_201.transform.translation.z = msg.pose.position.z; 
  ts_aruco_201.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_201.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_201.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_201.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 202 Pose subscribion
void aruco202Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_202.transform.translation.x = msg.pose.position.x;
  ts_aruco_202.transform.translation.y = msg.pose.position.y;
  ts_aruco_202.transform.translation.z = msg.pose.position.z; 
  ts_aruco_202.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_202.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_202.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_202.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 203 Pose subscribion
void aruco203Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_203.transform.translation.x = msg.pose.position.x;
  ts_aruco_203.transform.translation.y = msg.pose.position.y;
  ts_aruco_203.transform.translation.z = msg.pose.position.z; 
  ts_aruco_203.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_203.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_203.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_203.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 204 Pose subscribion
void aruco204Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_204.transform.translation.x = msg.pose.position.x;
  ts_aruco_204.transform.translation.y = msg.pose.position.y;
  ts_aruco_204.transform.translation.z = msg.pose.position.z; 
  ts_aruco_204.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_204.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_204.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_204.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 205 Pose subscribion
void aruco205Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_205.transform.translation.x = msg.pose.position.x;
  ts_aruco_205.transform.translation.y = msg.pose.position.y;
  ts_aruco_205.transform.translation.z = msg.pose.position.z; 
  ts_aruco_205.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_205.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_205.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_205.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 206 Pose subscribion
void aruco206Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_206.transform.translation.x = msg.pose.position.x;
  ts_aruco_206.transform.translation.y = msg.pose.position.y;
  ts_aruco_206.transform.translation.z = msg.pose.position.z; 
  ts_aruco_206.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_206.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_206.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_206.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 207 Pose subscribion
void aruco207Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_207.transform.translation.x = msg.pose.position.x;
  ts_aruco_207.transform.translation.y = msg.pose.position.y;
  ts_aruco_207.transform.translation.z = msg.pose.position.z; 
  ts_aruco_207.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_207.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_207.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_207.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 208 Pose subscribion
void aruco208Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_208.transform.translation.x = msg.pose.position.x;
  ts_aruco_208.transform.translation.y = msg.pose.position.y;
  ts_aruco_208.transform.translation.z = msg.pose.position.z; 
  ts_aruco_208.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_208.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_208.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_208.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 209 Pose subscribion
void aruco209Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_209.transform.translation.x = msg.pose.position.x;
  ts_aruco_209.transform.translation.y = msg.pose.position.y;
  ts_aruco_209.transform.translation.z = msg.pose.position.z; 
  ts_aruco_209.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_209.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_209.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_209.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 210 Pose subscribion
void aruco210Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_210.transform.translation.x = msg.pose.position.x;
  ts_aruco_210.transform.translation.y = msg.pose.position.y;
  ts_aruco_210.transform.translation.z = msg.pose.position.z; 
  ts_aruco_210.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_210.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_210.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_210.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 211 Pose subscribion
void aruco211Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_211.transform.translation.x = msg.pose.position.x;
  ts_aruco_211.transform.translation.y = msg.pose.position.y;
  ts_aruco_211.transform.translation.z = msg.pose.position.z; 
  ts_aruco_211.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_211.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_211.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_211.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 212 Pose subscribion
void aruco212Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_212.transform.translation.x = msg.pose.position.x;
  ts_aruco_212.transform.translation.y = msg.pose.position.y;
  ts_aruco_212.transform.translation.z = msg.pose.position.z; 
  ts_aruco_212.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_212.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_212.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_212.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 213 Pose subscribion
void aruco213Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_213.transform.translation.x = msg.pose.position.x;
  ts_aruco_213.transform.translation.y = msg.pose.position.y;
  ts_aruco_213.transform.translation.z = msg.pose.position.z; 
  ts_aruco_213.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_213.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_213.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_213.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 214 Pose subscribion
void aruco214Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_214.transform.translation.x = msg.pose.position.x;
  ts_aruco_214.transform.translation.y = msg.pose.position.y;
  ts_aruco_214.transform.translation.z = msg.pose.position.z; 
  ts_aruco_214.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_214.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_214.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_214.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 215 Pose subscribion
void aruco215Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_215.transform.translation.x = msg.pose.position.x;
  ts_aruco_215.transform.translation.y = msg.pose.position.y;
  ts_aruco_215.transform.translation.z = msg.pose.position.z; 
  ts_aruco_215.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_215.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_215.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_215.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 216 Pose subscribion
void aruco216Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_216.transform.translation.x = msg.pose.position.x;
  ts_aruco_216.transform.translation.y = msg.pose.position.y;
  ts_aruco_216.transform.translation.z = msg.pose.position.z; 
  ts_aruco_216.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_216.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_216.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_216.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 301 Pose subscribion
void aruco301Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_301.transform.translation.x = msg.pose.position.x;
  ts_aruco_301.transform.translation.y = msg.pose.position.y;
  ts_aruco_301.transform.translation.z = msg.pose.position.z; 
  ts_aruco_301.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_301.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_301.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_301.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 302 Pose subscribion
void aruco302Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_302.transform.translation.x = msg.pose.position.x;
  ts_aruco_302.transform.translation.y = msg.pose.position.y;
  ts_aruco_302.transform.translation.z = msg.pose.position.z; 
  ts_aruco_302.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_302.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_302.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_302.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 303 Pose subscribion
void aruco303Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_303.transform.translation.x = msg.pose.position.x;
  ts_aruco_303.transform.translation.y = msg.pose.position.y;
  ts_aruco_303.transform.translation.z = msg.pose.position.z; 
  ts_aruco_303.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_303.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_303.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_303.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 304 Pose subscribion
void aruco304Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_304.transform.translation.x = msg.pose.position.x;
  ts_aruco_304.transform.translation.y = msg.pose.position.y;
  ts_aruco_304.transform.translation.z = msg.pose.position.z; 
  ts_aruco_304.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_304.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_304.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_304.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 305 Pose subscribion
void aruco305Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_305.transform.translation.x = msg.pose.position.x;
  ts_aruco_305.transform.translation.y = msg.pose.position.y;
  ts_aruco_305.transform.translation.z = msg.pose.position.z; 
  ts_aruco_305.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_305.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_305.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_305.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 306 Pose subscribion
void aruco306Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_306.transform.translation.x = msg.pose.position.x;
  ts_aruco_306.transform.translation.y = msg.pose.position.y;
  ts_aruco_306.transform.translation.z = msg.pose.position.z; 
  ts_aruco_306.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_306.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_306.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_306.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 307 Pose subscribion
void aruco307Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_307.transform.translation.x = msg.pose.position.x;
  ts_aruco_307.transform.translation.y = msg.pose.position.y;
  ts_aruco_307.transform.translation.z = msg.pose.position.z; 
  ts_aruco_307.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_307.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_307.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_307.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 308 Pose subscribion
void aruco308Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_308.transform.translation.x = msg.pose.position.x;
  ts_aruco_308.transform.translation.y = msg.pose.position.y;
  ts_aruco_308.transform.translation.z = msg.pose.position.z; 
  ts_aruco_308.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_308.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_308.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_308.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 309 Pose subscribion
void aruco309Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_309.transform.translation.x = msg.pose.position.x;
  ts_aruco_309.transform.translation.y = msg.pose.position.y;
  ts_aruco_309.transform.translation.z = msg.pose.position.z; 
  ts_aruco_309.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_309.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_309.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_309.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 310 Pose subscribion
void aruco310Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_310.transform.translation.x = msg.pose.position.x;
  ts_aruco_310.transform.translation.y = msg.pose.position.y;
  ts_aruco_310.transform.translation.z = msg.pose.position.z; 
  ts_aruco_310.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_310.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_310.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_310.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 311 Pose subscribion
void aruco311Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_311.transform.translation.x = msg.pose.position.x;
  ts_aruco_311.transform.translation.y = msg.pose.position.y;
  ts_aruco_311.transform.translation.z = msg.pose.position.z; 
  ts_aruco_311.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_311.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_311.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_311.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 312 Pose subscribion
void aruco312Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_312.transform.translation.x = msg.pose.position.x;
  ts_aruco_312.transform.translation.y = msg.pose.position.y;
  ts_aruco_312.transform.translation.z = msg.pose.position.z; 
  ts_aruco_312.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_312.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_312.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_312.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 313 Pose subscribion
void aruco313Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_313.transform.translation.x = msg.pose.position.x;
  ts_aruco_313.transform.translation.y = msg.pose.position.y;
  ts_aruco_313.transform.translation.z = msg.pose.position.z; 
  ts_aruco_313.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_313.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_313.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_313.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 314 Pose subscribion
void aruco314Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_314.transform.translation.x = msg.pose.position.x;
  ts_aruco_314.transform.translation.y = msg.pose.position.y;
  ts_aruco_314.transform.translation.z = msg.pose.position.z; 
  ts_aruco_314.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_314.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_314.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_314.transform.rotation.w = msg.pose.orientation.w;

}

// CallBack function Aruco 315 Pose subscribion
void aruco315Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_315.transform.translation.x = msg.pose.position.x;
  ts_aruco_315.transform.translation.y = msg.pose.position.y;
  ts_aruco_315.transform.translation.z = msg.pose.position.z; 
  ts_aruco_315.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_315.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_315.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_315.transform.rotation.w = msg.pose.orientation.w;

}


// CallBack function Aruco 316 Pose subscribion
void aruco316Callback(geometry_msgs::PoseStamped msg)
{
  ts_aruco_316.transform.translation.x = msg.pose.position.x;
  ts_aruco_316.transform.translation.y = msg.pose.position.y;
  ts_aruco_316.transform.translation.z = msg.pose.position.z; 
  ts_aruco_316.transform.rotation.x = msg.pose.orientation.x;
  ts_aruco_316.transform.rotation.y = msg.pose.orientation.y;
  ts_aruco_316.transform.rotation.z = msg.pose.orientation.z;
  ts_aruco_316.transform.rotation.w = msg.pose.orientation.w;

}




void boardpreloader()
{

	
	ts_A1.header.frame_id = "world";
  	ts_A1.child_frame_id = "A1_CELL";
  	ts_A1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A2.header.frame_id = "world";
  	ts_A2.child_frame_id = "A2_CELL";
  	ts_A2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A3.header.frame_id = "world";
  	ts_A3.child_frame_id = "A3_CELL";
  	ts_A3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A4.header.frame_id = "world";
  	ts_A4.child_frame_id = "A4_CELL";
  	ts_A4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A5.header.frame_id = "world";
  	ts_A5.child_frame_id = "A5_CELL";
  	ts_A5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A6.header.frame_id = "world";
  	ts_A6.child_frame_id = "A6_CELL";
  	ts_A6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A7.header.frame_id = "world";
  	ts_A7.child_frame_id = "A7_CELL";
  	ts_A7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_A8.header.frame_id = "world";
  	ts_A8.child_frame_id = "A8_CELL";
  	ts_A8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_A8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_A8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_A8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B1.header.frame_id = "world";
  	ts_B1.child_frame_id = "B1_CELL";
  	ts_B1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B2.header.frame_id = "world";
  	ts_B2.child_frame_id = "B2_CELL";
  	ts_B2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B3.header.frame_id = "world";
  	ts_B3.child_frame_id = "B3_CELL";
  	ts_B3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B4.header.frame_id = "world";
  	ts_B4.child_frame_id = "B4_CELL";
  	ts_B4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B5.header.frame_id = "world";
  	ts_B5.child_frame_id = "B5_CELL";
  	ts_B5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B6.header.frame_id = "world";
  	ts_B6.child_frame_id = "B6_CELL";
  	ts_B6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B7.header.frame_id = "world";
  	ts_B7.child_frame_id = "B7_CELL";
  	ts_B7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_B8.header.frame_id = "world";
  	ts_B8.child_frame_id = "B8_CELL";
  	ts_B8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_B8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_B8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_B8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C1.header.frame_id = "world";
  	ts_C1.child_frame_id = "C1_CELL";
  	ts_C1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C2.header.frame_id = "world";
  	ts_C2.child_frame_id = "C2_CELL";
  	ts_C2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C3.header.frame_id = "world";
  	ts_C3.child_frame_id = "C3_CELL";
  	ts_C3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C4.header.frame_id = "world";
  	ts_C4.child_frame_id = "C4_CELL";
  	ts_C4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C5.header.frame_id = "world";
  	ts_C5.child_frame_id = "C5_CELL";
  	ts_C5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C6.header.frame_id = "world";
  	ts_C6.child_frame_id = "C6_CELL";
  	ts_C6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C7.header.frame_id = "world";
  	ts_C7.child_frame_id = "C7_CELL";
  	ts_C7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_C8.header.frame_id = "world";
  	ts_C8.child_frame_id = "C8_CELL";
  	ts_C8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_C8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_C8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_C8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D1.header.frame_id = "world";
  	ts_D1.child_frame_id = "D1_CELL";
  	ts_D1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D2.header.frame_id = "world";
  	ts_D2.child_frame_id = "D2_CELL";
  	ts_D2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D3.header.frame_id = "world";
  	ts_D3.child_frame_id = "D3_CELL";
  	ts_D3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D4.header.frame_id = "world";
  	ts_D4.child_frame_id = "D4_CELL";
  	ts_D4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D5.header.frame_id = "world";
  	ts_D5.child_frame_id = "D5_CELL";
  	ts_D5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D6.header.frame_id = "world";
  	ts_D6.child_frame_id = "D6_CELL";
  	ts_D6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D7.header.frame_id = "world";
  	ts_D7.child_frame_id = "D7_CELL";
  	ts_D7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_D8.header.frame_id = "world";
  	ts_D8.child_frame_id = "D8_CELL";
  	ts_D8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_D8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_D8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_D8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E1.header.frame_id = "world";
  	ts_E1.child_frame_id = "E1_CELL";
  	ts_E1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E2.header.frame_id = "world";
  	ts_E2.child_frame_id = "E2_CELL";
  	ts_E2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E3.header.frame_id = "world";
  	ts_E3.child_frame_id = "E3_CELL";
  	ts_E3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E4.header.frame_id = "world";
  	ts_E4.child_frame_id = "E4_CELL";
  	ts_E4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E5.header.frame_id = "world";
  	ts_E5.child_frame_id = "E5_CELL";
  	ts_E5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E6.header.frame_id = "world";
  	ts_E6.child_frame_id = "E6_CELL";
  	ts_E6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E7.header.frame_id = "world";
  	ts_E7.child_frame_id = "E7_CELL";
  	ts_E7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_E8.header.frame_id = "world";
  	ts_E8.child_frame_id = "E8_CELL";
  	ts_E8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_E8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_E8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_E8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F1.header.frame_id = "world";
  	ts_F1.child_frame_id = "F1_CELL";
  	ts_F1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F2.header.frame_id = "world";
  	ts_F2.child_frame_id = "F2_CELL";
  	ts_F2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F3.header.frame_id = "world";
  	ts_F3.child_frame_id = "F3_CELL";
  	ts_F3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F4.header.frame_id = "world";
  	ts_F4.child_frame_id = "F4_CELL";
  	ts_F4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F5.header.frame_id = "world";
  	ts_F5.child_frame_id = "F5_CELL";
  	ts_F5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F6.header.frame_id = "world";
  	ts_F6.child_frame_id = "F6_CELL";
  	ts_F6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F7.header.frame_id = "world";
  	ts_F7.child_frame_id = "F7_CELL";
  	ts_F7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_F8.header.frame_id = "world";
  	ts_F8.child_frame_id = "F8_CELL";
  	ts_F8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_F8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_F8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_F8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G1.header.frame_id = "world";
  	ts_G1.child_frame_id = "G1_CELL";
  	ts_G1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G2.header.frame_id = "world";
  	ts_G2.child_frame_id = "G2_CELL";
  	ts_G2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G3.header.frame_id = "world";
  	ts_G3.child_frame_id = "G3_CELL";
  	ts_G3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G4.header.frame_id = "world";
  	ts_G4.child_frame_id = "G4_CELL";
  	ts_G4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G5.header.frame_id = "world";
  	ts_G5.child_frame_id = "G5_CELL";
  	ts_G5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G6.header.frame_id = "world";
  	ts_G6.child_frame_id = "G6_CELL";
  	ts_G6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G7.header.frame_id = "world";
  	ts_G7.child_frame_id = "G7_CELL";
  	ts_G7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_G8.header.frame_id = "world";
  	ts_G8.child_frame_id = "G8_CELL";
  	ts_G8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_G8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_G8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_G8.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H1.header.frame_id = "world";
  	ts_H1.child_frame_id = "H1_CELL";
  	ts_H1.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H1.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H1.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H1.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H2.header.frame_id = "world";
  	ts_H2.child_frame_id = "H2_CELL";
  	ts_H2.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H2.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H2.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H2.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H3.header.frame_id = "world";
  	ts_H3.child_frame_id = "H3_CELL";
  	ts_H3.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H3.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H3.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H3.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H4.header.frame_id = "world";
  	ts_H4.child_frame_id = "H4_CELL";
  	ts_H4.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H4.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H4.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H4.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H5.header.frame_id = "world";
  	ts_H5.child_frame_id = "H5_CELL";
  	ts_H5.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H5.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H5.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H5.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H6.header.frame_id = "world";
  	ts_H6.child_frame_id = "H6_CELL";
  	ts_H6.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H6.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H6.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H6.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H7.header.frame_id = "world";
  	ts_H7.child_frame_id = "H7_CELL";
  	ts_H7.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H7.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H7.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H7.transform.rotation.w = ts_aruco_102.transform.rotation.w;

	ts_H8.header.frame_id = "world";
  	ts_H8.child_frame_id = "H8_CELL";
  	ts_H8.transform.rotation.x = ts_aruco_102.transform.rotation.x;
  	ts_H8.transform.rotation.y = ts_aruco_102.transform.rotation.y;
  	ts_H8.transform.rotation.z = ts_aruco_102.transform.rotation.z;
  	ts_H8.transform.rotation.w = ts_aruco_102.transform.rotation.w;
}

void boardloader()
{

        lengthx = ((sqrt((c100x-c102x)*(c100x-c102x))-(2.65/100)) + (sqrt((c101x-c103x)*(c101x-c103x))-(2.65/100)))/2;
	lengthy = ((sqrt((c100y-c101y)*(c100y-c101y))-(2.65/100)) + (sqrt((c102y-c103y)*(c102y-c103y))-(2.65/100)))/2;
	incrx = lengthx/8;
        incry = lengthy/8;
 
  	ts_A1.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_A1.transform.translation.z = 0;	
  	
  	ts_A2.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_A2.transform.translation.z = 0;	
  	
  	ts_A3.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_A3.transform.translation.z = 0;	
  	
  	ts_A4.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_A4.transform.translation.z = 0;	
  	
  	ts_A5.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_A5.transform.translation.z = 0;	
  	
  	ts_A6.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_A6.transform.translation.z = 0;	
  	
  	ts_A7.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_A7.transform.translation.z = 0;	
  	
  	ts_A8.transform.translation.y = (c102x-(2.65/200))-(incrx*1)+(incrx/2);
  	ts_A8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_A8.transform.translation.z = 0;	
  	
  	ts_B1.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_B1.transform.translation.z = 0;	
  	
  	ts_B2.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_B2.transform.translation.z = 0;	
  
  	ts_B3.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_B3.transform.translation.z = 0;	
  	
  	ts_B4.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_B4.transform.translation.z = 0;	
  	
  	ts_B5.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_B5.transform.translation.z = 0;	
  	
  	ts_B6.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_B6.transform.translation.z = 0;	
  	
  	ts_B7.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_B7.transform.translation.z = 0;	
  	
  	ts_B8.transform.translation.y = (c102x-(2.65/200))-(incrx*2)+(incrx/2);
  	ts_B8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_B8.transform.translation.z = 0;

  	ts_C1.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_C1.transform.translation.z = 0;	
  	
  	ts_C2.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_C2.transform.translation.z = 0;	
  	
  	ts_C3.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_C3.transform.translation.z = 0;	
  	
  	ts_C4.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_C4.transform.translation.z = 0;	
  	
  	ts_C5.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_C5.transform.translation.z = 0;	
  	
  	ts_C6.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_C6.transform.translation.z = 0;	
  	
  	ts_C7.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_C7.transform.translation.z = 0;	
  	
  	ts_C8.transform.translation.y = (c102x-(2.65/200))-(incrx*3)+(incrx/2);
  	ts_C8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_C8.transform.translation.z = 0;	
  	
  	ts_D1.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_D1.transform.translation.z = 0;	
  	
  	ts_D2.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_D2.transform.translation.z = 0;	
  
  	ts_D3.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_D3.transform.translation.z = 0;	
  	
  	ts_D4.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_D4.transform.translation.z = 0;	
  	
  	ts_D5.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_D5.transform.translation.z = 0;	
  	
  	ts_D6.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_D6.transform.translation.z = 0;	
  	
  	ts_D7.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_D7.transform.translation.z = 0;	
  	
  	ts_D8.transform.translation.y = (c102x-(2.65/200))-(incrx*4)+(incrx/2);
  	ts_D8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_D8.transform.translation.z = 0;

  	ts_E1.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_E1.transform.translation.z = 0;	
  	
  	ts_E2.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_E2.transform.translation.z = 0;	
  	
  	ts_E3.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_E3.transform.translation.z = 0;	
  	
  	ts_E4.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_E4.transform.translation.z = 0;	
  	
  	ts_E5.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_E5.transform.translation.z = 0;	
  	
  	ts_E6.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_E6.transform.translation.z = 0;	
  	
  	ts_E7.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_E7.transform.translation.z = 0;	
  	
  	ts_E8.transform.translation.y = (c102x-(2.65/200))-(incrx*5)+(incrx/2);
  	ts_E8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_E8.transform.translation.z = 0;	
  	
  	ts_F1.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_F1.transform.translation.z = 0;	
  	
  	ts_F2.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_F2.transform.translation.z = 0;	
  
  	ts_F3.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_F3.transform.translation.z = 0;	
  	
  	ts_F4.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_F4.transform.translation.z = 0;	
  	
  	ts_F5.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_F5.transform.translation.z = 0;	
  	
  	ts_F6.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_F6.transform.translation.z = 0;	
  	
  	ts_F7.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_F7.transform.translation.z = 0;	
  	
  	ts_F8.transform.translation.y = (c102x-(2.65/200))-(incrx*6)+(incrx/2);
  	ts_F8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_F8.transform.translation.z = 0;

  	ts_G1.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_G1.transform.translation.z = 0;	
  	
  	ts_G2.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_G2.transform.translation.z = 0;	
  	
  	ts_G3.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_G3.transform.translation.z = 0;	
  	
  	ts_G4.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_G4.transform.translation.z = 0;	
  	
  	ts_G5.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_G5.transform.translation.z = 0;	
  	
  	ts_G6.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_G6.transform.translation.z = 0;	
  	
  	ts_G7.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_G7.transform.translation.z = 0;	
  	
  	ts_G8.transform.translation.y = (c102x-(2.65/200))-(incrx*7)+(incrx/2);
  	ts_G8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_G8.transform.translation.z = 0;	
  	
  	ts_H1.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H1.transform.translation.x = (c102y+(2.65/200))+(incry*1)-(incry/2);
  	ts_H1.transform.translation.z = 0;	
  	
  	ts_H2.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H2.transform.translation.x = (c102y+(2.65/200))+(incry*2)-(incry/2);
  	ts_H2.transform.translation.z = 0;	
  
  	ts_H3.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H3.transform.translation.x = (c102y+(2.65/200))+(incry*3)-(incry/2);
  	ts_H3.transform.translation.z = 0;	
  	
  	ts_H4.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H4.transform.translation.x = (c102y+(2.65/200))+(incry*4)-(incry/2);
  	ts_H4.transform.translation.z = 0;	
  	
  	ts_H5.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H5.transform.translation.x = (c102y+(2.65/200))+(incry*5)-(incry/2);
  	ts_H5.transform.translation.z = 0;
  	
  	ts_H6.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H6.transform.translation.x = (c102y+(2.65/200))+(incry*6)-(incry/2);
  	ts_H6.transform.translation.z = 0;	
  	
  	ts_H7.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H7.transform.translation.x = (c102y+(2.65/200))+(incry*7)-(incry/2);
  	ts_H7.transform.translation.z = 0;	
  	
  	ts_H8.transform.translation.y = (c102x-(2.65/200))-(incrx*8)+(incrx/2);
  	ts_H8.transform.translation.x = (c102y+(2.65/200))+(incry*8)-(incry/2);
  	ts_H8.transform.translation.z = 0;

}



int main(int argc, char** argv){

  ros::init(argc, argv, "aruco_frames_detection");

  ros::NodeHandle n;

  tf2_ros::TransformBroadcaster tfb_aruco_100;
  tf2_ros::TransformBroadcaster tfb_aruco_101;
  tf2_ros::TransformBroadcaster tfb_aruco_102;
  tf2_ros::TransformBroadcaster tfb_aruco_103;

  tf2_ros::TransformBroadcaster tfb_aruco_201;
  tf2_ros::TransformBroadcaster tfb_aruco_202;
  tf2_ros::TransformBroadcaster tfb_aruco_203;
  tf2_ros::TransformBroadcaster tfb_aruco_204;
  tf2_ros::TransformBroadcaster tfb_aruco_205;
  tf2_ros::TransformBroadcaster tfb_aruco_206;
  tf2_ros::TransformBroadcaster tfb_aruco_207;
  tf2_ros::TransformBroadcaster tfb_aruco_208;
  tf2_ros::TransformBroadcaster tfb_aruco_209;
  tf2_ros::TransformBroadcaster tfb_aruco_210;
  tf2_ros::TransformBroadcaster tfb_aruco_211;
  tf2_ros::TransformBroadcaster tfb_aruco_212;
  tf2_ros::TransformBroadcaster tfb_aruco_213;
  tf2_ros::TransformBroadcaster tfb_aruco_214;
  tf2_ros::TransformBroadcaster tfb_aruco_215;
  tf2_ros::TransformBroadcaster tfb_aruco_216;

  tf2_ros::TransformBroadcaster tfb_aruco_301;
  tf2_ros::TransformBroadcaster tfb_aruco_302;
  tf2_ros::TransformBroadcaster tfb_aruco_303;
  tf2_ros::TransformBroadcaster tfb_aruco_304;
  tf2_ros::TransformBroadcaster tfb_aruco_305;
  tf2_ros::TransformBroadcaster tfb_aruco_306;
  tf2_ros::TransformBroadcaster tfb_aruco_307;
  tf2_ros::TransformBroadcaster tfb_aruco_308;
  tf2_ros::TransformBroadcaster tfb_aruco_309;
  tf2_ros::TransformBroadcaster tfb_aruco_310;
  tf2_ros::TransformBroadcaster tfb_aruco_311;
  tf2_ros::TransformBroadcaster tfb_aruco_312;
  tf2_ros::TransformBroadcaster tfb_aruco_313;
  tf2_ros::TransformBroadcaster tfb_aruco_314;
  tf2_ros::TransformBroadcaster tfb_aruco_315;
  tf2_ros::TransformBroadcaster tfb_aruco_316;

  tf2_ros::TransformBroadcaster tfb_A1;
  tf2_ros::TransformBroadcaster tfb_A2;
  tf2_ros::TransformBroadcaster tfb_A3;
  tf2_ros::TransformBroadcaster tfb_A4;
  tf2_ros::TransformBroadcaster tfb_A5;
  tf2_ros::TransformBroadcaster tfb_A6;
  tf2_ros::TransformBroadcaster tfb_A7;
  tf2_ros::TransformBroadcaster tfb_A8;

  tf2_ros::TransformBroadcaster tfb_B1;
  tf2_ros::TransformBroadcaster tfb_B2;
  tf2_ros::TransformBroadcaster tfb_B3;
  tf2_ros::TransformBroadcaster tfb_B4;
  tf2_ros::TransformBroadcaster tfb_B5;
  tf2_ros::TransformBroadcaster tfb_B6;
  tf2_ros::TransformBroadcaster tfb_B7;
  tf2_ros::TransformBroadcaster tfb_B8;

  tf2_ros::TransformBroadcaster tfb_C1;
  tf2_ros::TransformBroadcaster tfb_C2;
  tf2_ros::TransformBroadcaster tfb_C3;
  tf2_ros::TransformBroadcaster tfb_C4;
  tf2_ros::TransformBroadcaster tfb_C5;
  tf2_ros::TransformBroadcaster tfb_C6;
  tf2_ros::TransformBroadcaster tfb_C7;
  tf2_ros::TransformBroadcaster tfb_C8;

  tf2_ros::TransformBroadcaster tfb_D1;
  tf2_ros::TransformBroadcaster tfb_D2;
  tf2_ros::TransformBroadcaster tfb_D3;
  tf2_ros::TransformBroadcaster tfb_D4;
  tf2_ros::TransformBroadcaster tfb_D5;
  tf2_ros::TransformBroadcaster tfb_D6;
  tf2_ros::TransformBroadcaster tfb_D7;
  tf2_ros::TransformBroadcaster tfb_D8;

  tf2_ros::TransformBroadcaster tfb_E1;
  tf2_ros::TransformBroadcaster tfb_E2;
  tf2_ros::TransformBroadcaster tfb_E3;
  tf2_ros::TransformBroadcaster tfb_E4;
  tf2_ros::TransformBroadcaster tfb_E5;
  tf2_ros::TransformBroadcaster tfb_E6;
  tf2_ros::TransformBroadcaster tfb_E7;
  tf2_ros::TransformBroadcaster tfb_E8;

  tf2_ros::TransformBroadcaster tfb_F1;
  tf2_ros::TransformBroadcaster tfb_F2;
  tf2_ros::TransformBroadcaster tfb_F3;
  tf2_ros::TransformBroadcaster tfb_F4;
  tf2_ros::TransformBroadcaster tfb_F5;
  tf2_ros::TransformBroadcaster tfb_F6;
  tf2_ros::TransformBroadcaster tfb_F7;
  tf2_ros::TransformBroadcaster tfb_F8;

  tf2_ros::TransformBroadcaster tfb_G1;
  tf2_ros::TransformBroadcaster tfb_G2;
  tf2_ros::TransformBroadcaster tfb_G3;
  tf2_ros::TransformBroadcaster tfb_G4;
  tf2_ros::TransformBroadcaster tfb_G5;
  tf2_ros::TransformBroadcaster tfb_G6;
  tf2_ros::TransformBroadcaster tfb_G7;
  tf2_ros::TransformBroadcaster tfb_G8;

  tf2_ros::TransformBroadcaster tfb_H1;
  tf2_ros::TransformBroadcaster tfb_H2;
  tf2_ros::TransformBroadcaster tfb_H3;
  tf2_ros::TransformBroadcaster tfb_H4;
  tf2_ros::TransformBroadcaster tfb_H5;
  tf2_ros::TransformBroadcaster tfb_H6;
  tf2_ros::TransformBroadcaster tfb_H7;
  tf2_ros::TransformBroadcaster tfb_H8;

  // Subscribe to all the Aruco pose messages:
  ros::Subscriber sub_100 = n.subscribe("/aruco_single_100/pose", 1000, aruco100Callback);
  ros::Subscriber sub_101 = n.subscribe("/aruco_single_101/pose", 1000, aruco101Callback);
  ros::Subscriber sub_102 = n.subscribe("/aruco_single_102/pose", 1000, aruco102Callback);
  ros::Subscriber sub_103 = n.subscribe("/aruco_single_103/pose", 1000, aruco103Callback);



  ros::Subscriber sub_201 = n.subscribe("/aruco_single_201/pose", 1000, aruco201Callback);
  ros::Subscriber sub_202 = n.subscribe("/aruco_single_202/pose", 1000, aruco202Callback);
  ros::Subscriber sub_203 = n.subscribe("/aruco_single_203/pose", 1000, aruco203Callback);
  ros::Subscriber sub_204 = n.subscribe("/aruco_single_204/pose", 1000, aruco204Callback);
  ros::Subscriber sub_205 = n.subscribe("/aruco_single_205/pose", 1000, aruco205Callback);
  ros::Subscriber sub_206 = n.subscribe("/aruco_single_206/pose", 1000, aruco206Callback);
  ros::Subscriber sub_207 = n.subscribe("/aruco_single_207/pose", 1000, aruco207Callback);
  ros::Subscriber sub_208 = n.subscribe("/aruco_single_208/pose", 1000, aruco208Callback);
  ros::Subscriber sub_209 = n.subscribe("/aruco_single_209/pose", 1000, aruco209Callback);
  ros::Subscriber sub_210 = n.subscribe("/aruco_single_210/pose", 1000, aruco210Callback);
  ros::Subscriber sub_211 = n.subscribe("/aruco_single_211/pose", 1000, aruco211Callback);
  ros::Subscriber sub_212 = n.subscribe("/aruco_single_212/pose", 1000, aruco212Callback);
  ros::Subscriber sub_213 = n.subscribe("/aruco_single_213/pose", 1000, aruco213Callback);
  ros::Subscriber sub_214 = n.subscribe("/aruco_single_214/pose", 1000, aruco214Callback);
  ros::Subscriber sub_215 = n.subscribe("/aruco_single_215/pose", 1000, aruco215Callback);
  ros::Subscriber sub_216 = n.subscribe("/aruco_single_216/pose", 1000, aruco216Callback);

  ros::Subscriber sub_301 = n.subscribe("/aruco_single_301/pose", 1000, aruco301Callback);
  ros::Subscriber sub_302 = n.subscribe("/aruco_single_302/pose", 1000, aruco302Callback);
  ros::Subscriber sub_303 = n.subscribe("/aruco_single_303/pose", 1000, aruco303Callback);
  ros::Subscriber sub_304 = n.subscribe("/aruco_single_304/pose", 1000, aruco304Callback);
  ros::Subscriber sub_305 = n.subscribe("/aruco_single_305/pose", 1000, aruco305Callback);
  ros::Subscriber sub_306 = n.subscribe("/aruco_single_306/pose", 1000, aruco306Callback);
  ros::Subscriber sub_307 = n.subscribe("/aruco_single_307/pose", 1000, aruco307Callback);
  ros::Subscriber sub_308 = n.subscribe("/aruco_single_308/pose", 1000, aruco308Callback);
  ros::Subscriber sub_309 = n.subscribe("/aruco_single_309/pose", 1000, aruco309Callback);
  ros::Subscriber sub_310 = n.subscribe("/aruco_single_310/pose", 1000, aruco310Callback);
  ros::Subscriber sub_311 = n.subscribe("/aruco_single_311/pose", 1000, aruco311Callback);
  ros::Subscriber sub_312 = n.subscribe("/aruco_single_312/pose", 1000, aruco312Callback);
  ros::Subscriber sub_313 = n.subscribe("/aruco_single_313/pose", 1000, aruco313Callback);
  ros::Subscriber sub_314 = n.subscribe("/aruco_single_314/pose", 1000, aruco314Callback);
  ros::Subscriber sub_315 = n.subscribe("/aruco_single_315/pose", 1000, aruco315Callback);
  ros::Subscriber sub_316 = n.subscribe("/aruco_single_316/pose", 1000, aruco316Callback);

  ros::Duration(5.0).sleep();

  // Initialize all transformStamped messages.


  ts_aruco_100.header.frame_id = "world";
  ts_aruco_100.child_frame_id = "aruco_100";
  ts_aruco_100.transform.translation.x = 0;
  ts_aruco_100.transform.translation.y = 0;
  ts_aruco_100.transform.translation.z = 0;
  ts_aruco_100.transform.rotation.x = 0;
  ts_aruco_100.transform.rotation.y = 0;
  ts_aruco_100.transform.rotation.z = 0;
  ts_aruco_100.transform.rotation.w = 1;

  ts_aruco_101.header.frame_id = "world";
  ts_aruco_101.child_frame_id = "aruco_101";
  ts_aruco_101.transform.translation.x = 0;
  ts_aruco_101.transform.translation.y = 0;
  ts_aruco_101.transform.translation.z = 0;
  ts_aruco_101.transform.rotation.x = 0;
  ts_aruco_101.transform.rotation.y = 0;
  ts_aruco_101.transform.rotation.z = 0;
  ts_aruco_101.transform.rotation.w = 1;

  ts_aruco_102.header.frame_id = "world";
  ts_aruco_102.child_frame_id = "aruco_102";
  ts_aruco_102.transform.translation.x = 0;
  ts_aruco_102.transform.translation.y = 0;
  ts_aruco_102.transform.translation.z = 0;
  ts_aruco_102.transform.rotation.x = 0;
  ts_aruco_102.transform.rotation.y = 0;
  ts_aruco_102.transform.rotation.z = 0;
  ts_aruco_102.transform.rotation.w = 1;

  ts_aruco_103.header.frame_id = "world";
  ts_aruco_103.child_frame_id = "aruco_103";
  ts_aruco_103.transform.translation.x = 0;
  ts_aruco_103.transform.translation.y = 0;
  ts_aruco_103.transform.translation.z = 0;
  ts_aruco_103.transform.rotation.x = 0;
  ts_aruco_103.transform.rotation.y = 0;
  ts_aruco_103.transform.rotation.z = 0;
  ts_aruco_103.transform.rotation.w = 1;



  ts_aruco_201.header.frame_id = "world";
  ts_aruco_201.child_frame_id = "aruco_201";
  ts_aruco_201.transform.translation.x = 0;
  ts_aruco_201.transform.translation.y = 0;
  ts_aruco_201.transform.translation.z = 0;
  ts_aruco_201.transform.rotation.x = 0;
  ts_aruco_201.transform.rotation.y = 0;
  ts_aruco_201.transform.rotation.z = 0;
  ts_aruco_201.transform.rotation.w = 1;

  ts_aruco_202.header.frame_id = "world";
  ts_aruco_202.child_frame_id = "aruco_202";
  ts_aruco_202.transform.translation.x = 0;
  ts_aruco_202.transform.translation.y = 0;
  ts_aruco_202.transform.translation.z = 0;
  ts_aruco_202.transform.rotation.x = 0;
  ts_aruco_202.transform.rotation.y = 0;
  ts_aruco_202.transform.rotation.z = 0;
  ts_aruco_202.transform.rotation.w = 1;

  ts_aruco_203.header.frame_id = "world";
  ts_aruco_203.child_frame_id = "aruco_203";
  ts_aruco_203.transform.translation.x = 0;
  ts_aruco_203.transform.translation.y = 0;
  ts_aruco_203.transform.translation.z = 0;
  ts_aruco_203.transform.rotation.x = 0;
  ts_aruco_203.transform.rotation.y = 0;
  ts_aruco_203.transform.rotation.z = 0;
  ts_aruco_203.transform.rotation.w = 1;

  ts_aruco_204.header.frame_id = "world";
  ts_aruco_204.child_frame_id = "aruco_204";
  ts_aruco_204.transform.translation.x = 0;
  ts_aruco_204.transform.translation.y = 0;
  ts_aruco_204.transform.translation.z = 0;
  ts_aruco_204.transform.rotation.x = 0;
  ts_aruco_204.transform.rotation.y = 0;
  ts_aruco_204.transform.rotation.z = 0;
  ts_aruco_204.transform.rotation.w = 1;

  ts_aruco_205.header.frame_id = "world";
  ts_aruco_205.child_frame_id = "aruco_205";
  ts_aruco_205.transform.translation.x = 0;
  ts_aruco_205.transform.translation.y = 0;
  ts_aruco_205.transform.translation.z = 0;
  ts_aruco_205.transform.rotation.x = 0;
  ts_aruco_205.transform.rotation.y = 0;
  ts_aruco_205.transform.rotation.z = 0;
  ts_aruco_205.transform.rotation.w = 1;

  ts_aruco_206.header.frame_id = "world";
  ts_aruco_206.child_frame_id = "aruco_206";
  ts_aruco_206.transform.translation.x = 0;
  ts_aruco_206.transform.translation.y = 0;
  ts_aruco_206.transform.translation.z = 0;
  ts_aruco_206.transform.rotation.x = 0;
  ts_aruco_206.transform.rotation.y = 0;
  ts_aruco_206.transform.rotation.z = 0;
  ts_aruco_206.transform.rotation.w = 1;

  ts_aruco_207.header.frame_id = "world";
  ts_aruco_207.child_frame_id = "aruco_207";
  ts_aruco_207.transform.translation.x = 0;
  ts_aruco_207.transform.translation.y = 0;
  ts_aruco_207.transform.translation.z = 0;
  ts_aruco_207.transform.rotation.x = 0;
  ts_aruco_207.transform.rotation.y = 0;
  ts_aruco_207.transform.rotation.z = 0;
  ts_aruco_207.transform.rotation.w = 1;

  ts_aruco_208.header.frame_id = "world";
  ts_aruco_208.child_frame_id = "aruco_208";
  ts_aruco_208.transform.translation.x = 0;
  ts_aruco_208.transform.translation.y = 0;
  ts_aruco_208.transform.translation.z = 0;
  ts_aruco_208.transform.rotation.x = 0;
  ts_aruco_208.transform.rotation.y = 0;
  ts_aruco_208.transform.rotation.z = 0;
  ts_aruco_208.transform.rotation.w = 1;

  ts_aruco_209.header.frame_id = "world";
  ts_aruco_209.child_frame_id = "aruco_209";
  ts_aruco_209.transform.translation.x = 0;
  ts_aruco_209.transform.translation.y = 0;
  ts_aruco_209.transform.translation.z = 0;
  ts_aruco_209.transform.rotation.x = 0;
  ts_aruco_209.transform.rotation.y = 0;
  ts_aruco_209.transform.rotation.z = 0;
  ts_aruco_209.transform.rotation.w = 1;

  ts_aruco_210.header.frame_id = "world";
  ts_aruco_210.child_frame_id = "aruco_210";
  ts_aruco_210.transform.translation.x = 0;
  ts_aruco_210.transform.translation.y = 0;
  ts_aruco_210.transform.translation.z = 0;
  ts_aruco_210.transform.rotation.x = 0;
  ts_aruco_210.transform.rotation.y = 0;
  ts_aruco_210.transform.rotation.z = 0;
  ts_aruco_210.transform.rotation.w = 1;

  ts_aruco_211.header.frame_id = "world";
  ts_aruco_211.child_frame_id = "aruco_211";
  ts_aruco_211.transform.translation.x = 0;
  ts_aruco_211.transform.translation.y = 0;
  ts_aruco_211.transform.translation.z = 0;
  ts_aruco_211.transform.rotation.x = 0;
  ts_aruco_211.transform.rotation.y = 0;
  ts_aruco_211.transform.rotation.z = 0;
  ts_aruco_211.transform.rotation.w = 1;

  ts_aruco_212.header.frame_id = "world";
  ts_aruco_212.child_frame_id = "aruco_212";
  ts_aruco_212.transform.translation.x = 0;
  ts_aruco_212.transform.translation.y = 0;
  ts_aruco_212.transform.translation.z = 0;
  ts_aruco_212.transform.rotation.x = 0;
  ts_aruco_212.transform.rotation.y = 0;
  ts_aruco_212.transform.rotation.z = 0;
  ts_aruco_212.transform.rotation.w = 1;

  ts_aruco_213.header.frame_id = "world";
  ts_aruco_213.child_frame_id = "aruco_213";
  ts_aruco_213.transform.translation.x = 0;
  ts_aruco_213.transform.translation.y = 0;
  ts_aruco_213.transform.translation.z = 0;
  ts_aruco_213.transform.rotation.x = 0;
  ts_aruco_213.transform.rotation.y = 0;
  ts_aruco_213.transform.rotation.z = 0;
  ts_aruco_213.transform.rotation.w = 1;

  ts_aruco_214.header.frame_id = "world";
  ts_aruco_214.child_frame_id = "aruco_214";
  ts_aruco_214.transform.translation.x = 0;
  ts_aruco_214.transform.translation.y = 0;
  ts_aruco_214.transform.translation.z = 0;
  ts_aruco_214.transform.rotation.x = 0;
  ts_aruco_214.transform.rotation.y = 0;
  ts_aruco_214.transform.rotation.z = 0;
  ts_aruco_214.transform.rotation.w = 1;

  ts_aruco_215.header.frame_id = "world";
  ts_aruco_215.child_frame_id = "aruco_215";
  ts_aruco_215.transform.translation.x = 0;
  ts_aruco_215.transform.translation.y = 0;
  ts_aruco_215.transform.translation.z = 0;
  ts_aruco_215.transform.rotation.x = 0;
  ts_aruco_215.transform.rotation.y = 0;
  ts_aruco_215.transform.rotation.z = 0;
  ts_aruco_215.transform.rotation.w = 1;

  ts_aruco_216.header.frame_id = "world";
  ts_aruco_216.child_frame_id = "aruco_216";
  ts_aruco_216.transform.translation.x = 0;
  ts_aruco_216.transform.translation.y = 0;
  ts_aruco_216.transform.translation.z = 0;
  ts_aruco_216.transform.rotation.x = 0;
  ts_aruco_216.transform.rotation.y = 0;
  ts_aruco_216.transform.rotation.z = 0;
  ts_aruco_216.transform.rotation.w = 1;

  ts_aruco_301.header.frame_id = "world";
  ts_aruco_301.child_frame_id = "aruco_301";
  ts_aruco_301.transform.translation.x = 0;
  ts_aruco_301.transform.translation.y = 0;
  ts_aruco_301.transform.translation.z = 0;
  ts_aruco_301.transform.rotation.x = 0;
  ts_aruco_301.transform.rotation.y = 0;
  ts_aruco_301.transform.rotation.z = 0;
  ts_aruco_301.transform.rotation.w = 1;

  ts_aruco_302.header.frame_id = "world";
  ts_aruco_302.child_frame_id = "aruco_302";
  ts_aruco_302.transform.translation.x = 0;
  ts_aruco_302.transform.translation.y = 0;
  ts_aruco_302.transform.translation.z = 0;
  ts_aruco_302.transform.rotation.x = 0;
  ts_aruco_302.transform.rotation.y = 0;
  ts_aruco_302.transform.rotation.z = 0;
  ts_aruco_302.transform.rotation.w = 1;

  ts_aruco_303.header.frame_id = "world";
  ts_aruco_303.child_frame_id = "aruco_303";
  ts_aruco_303.transform.translation.x = 0;
  ts_aruco_303.transform.translation.y = 0;
  ts_aruco_303.transform.translation.z = 0;
  ts_aruco_303.transform.rotation.x = 0;
  ts_aruco_303.transform.rotation.y = 0;
  ts_aruco_303.transform.rotation.z = 0;
  ts_aruco_303.transform.rotation.w = 1;

  ts_aruco_304.header.frame_id = "world";
  ts_aruco_304.child_frame_id = "aruco_304";
  ts_aruco_304.transform.translation.x = 0;
  ts_aruco_304.transform.translation.y = 0;
  ts_aruco_304.transform.translation.z = 0;
  ts_aruco_304.transform.rotation.x = 0;
  ts_aruco_304.transform.rotation.y = 0;
  ts_aruco_304.transform.rotation.z = 0;
  ts_aruco_304.transform.rotation.w = 1;

  ts_aruco_305.header.frame_id = "world";
  ts_aruco_305.child_frame_id = "aruco_305";
  ts_aruco_305.transform.translation.x = 0;
  ts_aruco_305.transform.translation.y = 0;
  ts_aruco_305.transform.translation.z = 0;
  ts_aruco_305.transform.rotation.x = 0;
  ts_aruco_305.transform.rotation.y = 0;
  ts_aruco_305.transform.rotation.z = 0;
  ts_aruco_305.transform.rotation.w = 1;

  ts_aruco_306.header.frame_id = "world";
  ts_aruco_306.child_frame_id = "aruco_306";
  ts_aruco_306.transform.translation.x = 0;
  ts_aruco_306.transform.translation.y = 0;
  ts_aruco_306.transform.translation.z = 0;
  ts_aruco_306.transform.rotation.x = 0;
  ts_aruco_306.transform.rotation.y = 0;
  ts_aruco_306.transform.rotation.z = 0;
  ts_aruco_306.transform.rotation.w = 1;

  ts_aruco_307.header.frame_id = "world";
  ts_aruco_307.child_frame_id = "aruco_307";
  ts_aruco_307.transform.translation.x = 0;
  ts_aruco_307.transform.translation.y = 0;
  ts_aruco_307.transform.translation.z = 0;
  ts_aruco_307.transform.rotation.x = 0;
  ts_aruco_307.transform.rotation.y = 0;
  ts_aruco_307.transform.rotation.z = 0;
  ts_aruco_307.transform.rotation.w = 1;

  ts_aruco_308.header.frame_id = "world";
  ts_aruco_308.child_frame_id = "aruco_308";
  ts_aruco_308.transform.translation.x = 0;
  ts_aruco_308.transform.translation.y = 0;
  ts_aruco_308.transform.translation.z = 0;
  ts_aruco_308.transform.rotation.x = 0;
  ts_aruco_308.transform.rotation.y = 0;
  ts_aruco_308.transform.rotation.z = 0;
  ts_aruco_308.transform.rotation.w = 1;

  ts_aruco_309.header.frame_id = "world";
  ts_aruco_309.child_frame_id = "aruco_309";
  ts_aruco_309.transform.translation.x = 0;
  ts_aruco_309.transform.translation.y = 0;
  ts_aruco_309.transform.translation.z = 0;
  ts_aruco_309.transform.rotation.x = 0;
  ts_aruco_309.transform.rotation.y = 0;
  ts_aruco_309.transform.rotation.z = 0;
  ts_aruco_309.transform.rotation.w = 1;

  ts_aruco_310.header.frame_id = "world";
  ts_aruco_310.child_frame_id = "aruco_310";
  ts_aruco_310.transform.translation.x = 0;
  ts_aruco_310.transform.translation.y = 0;
  ts_aruco_310.transform.translation.z = 0;
  ts_aruco_310.transform.rotation.x = 0;
  ts_aruco_310.transform.rotation.y = 0;
  ts_aruco_310.transform.rotation.z = 0;
  ts_aruco_310.transform.rotation.w = 1;

  ts_aruco_311.header.frame_id = "world";
  ts_aruco_311.child_frame_id = "aruco_311";
  ts_aruco_311.transform.translation.x = 0;
  ts_aruco_311.transform.translation.y = 0;
  ts_aruco_311.transform.translation.z = 0;
  ts_aruco_311.transform.rotation.x = 0;
  ts_aruco_311.transform.rotation.y = 0;
  ts_aruco_311.transform.rotation.z = 0;
  ts_aruco_311.transform.rotation.w = 1;

  ts_aruco_312.header.frame_id = "world";
  ts_aruco_312.child_frame_id = "aruco_312";
  ts_aruco_312.transform.translation.x = 0;
  ts_aruco_312.transform.translation.y = 0;
  ts_aruco_312.transform.translation.z = 0;
  ts_aruco_312.transform.rotation.x = 0;
  ts_aruco_312.transform.rotation.y = 0;
  ts_aruco_312.transform.rotation.z = 0;
  ts_aruco_312.transform.rotation.w = 1;

  ts_aruco_313.header.frame_id = "world";
  ts_aruco_313.child_frame_id = "aruco_313";
  ts_aruco_313.transform.translation.x = 0;
  ts_aruco_313.transform.translation.y = 0;
  ts_aruco_313.transform.translation.z = 0;
  ts_aruco_313.transform.rotation.x = 0;
  ts_aruco_313.transform.rotation.y = 0;
  ts_aruco_313.transform.rotation.z = 0;
  ts_aruco_313.transform.rotation.w = 1;

  ts_aruco_314.header.frame_id = "world";
  ts_aruco_314.child_frame_id = "aruco_314";
  ts_aruco_314.transform.translation.x = 0;
  ts_aruco_314.transform.translation.y = 0;
  ts_aruco_314.transform.translation.z = 0;
  ts_aruco_314.transform.rotation.x = 0;
  ts_aruco_314.transform.rotation.y = 0;
  ts_aruco_314.transform.rotation.z = 0;
  ts_aruco_314.transform.rotation.w = 1;

  ts_aruco_315.header.frame_id = "world";
  ts_aruco_315.child_frame_id = "aruco_315";
  ts_aruco_315.transform.translation.x = 0;
  ts_aruco_315.transform.translation.y = 0;
  ts_aruco_315.transform.translation.z = 0;
  ts_aruco_315.transform.rotation.x = 0;
  ts_aruco_315.transform.rotation.y = 0;
  ts_aruco_315.transform.rotation.z = 0;
  ts_aruco_315.transform.rotation.w = 1;

  ts_aruco_316.header.frame_id = "world";
  ts_aruco_316.child_frame_id = "aruco_316";
  ts_aruco_316.transform.translation.x = 0;
  ts_aruco_316.transform.translation.y = 0;
  ts_aruco_316.transform.translation.z = 0;
  ts_aruco_316.transform.rotation.x = 0;
  ts_aruco_316.transform.rotation.y = 0;
  ts_aruco_316.transform.rotation.z = 0;
  ts_aruco_316.transform.rotation.w = 1;

  boardpreloader();
  boardloader();
  ros::Rate rate(30.0);
  

  while (n.ok()){

    boardloader();
    tfb_aruco_100.sendTransform(ts_aruco_100);
    tfb_aruco_101.sendTransform(ts_aruco_101);
    tfb_aruco_102.sendTransform(ts_aruco_102);
    tfb_aruco_103.sendTransform(ts_aruco_103);


    tfb_aruco_201.sendTransform(ts_aruco_201);
    tfb_aruco_202.sendTransform(ts_aruco_202);
    tfb_aruco_203.sendTransform(ts_aruco_203);
    tfb_aruco_204.sendTransform(ts_aruco_204);
    tfb_aruco_205.sendTransform(ts_aruco_205);
    tfb_aruco_206.sendTransform(ts_aruco_206);
    tfb_aruco_207.sendTransform(ts_aruco_207);
    tfb_aruco_208.sendTransform(ts_aruco_208);
    tfb_aruco_209.sendTransform(ts_aruco_209);
    tfb_aruco_210.sendTransform(ts_aruco_210);
    tfb_aruco_211.sendTransform(ts_aruco_211);
    tfb_aruco_212.sendTransform(ts_aruco_212);
    tfb_aruco_213.sendTransform(ts_aruco_213);
    tfb_aruco_214.sendTransform(ts_aruco_214);
    tfb_aruco_215.sendTransform(ts_aruco_215);
    tfb_aruco_216.sendTransform(ts_aruco_216);

    tfb_aruco_301.sendTransform(ts_aruco_301);
    tfb_aruco_302.sendTransform(ts_aruco_302);
    tfb_aruco_303.sendTransform(ts_aruco_303);
    tfb_aruco_304.sendTransform(ts_aruco_304);
    tfb_aruco_305.sendTransform(ts_aruco_305);
    tfb_aruco_306.sendTransform(ts_aruco_306);
    tfb_aruco_307.sendTransform(ts_aruco_307);
    tfb_aruco_308.sendTransform(ts_aruco_308);
    tfb_aruco_309.sendTransform(ts_aruco_309);
    tfb_aruco_310.sendTransform(ts_aruco_310);
    tfb_aruco_311.sendTransform(ts_aruco_311);
    tfb_aruco_312.sendTransform(ts_aruco_312);
    tfb_aruco_313.sendTransform(ts_aruco_313);
    tfb_aruco_314.sendTransform(ts_aruco_314);
    tfb_aruco_315.sendTransform(ts_aruco_315);
    tfb_aruco_316.sendTransform(ts_aruco_316);

    tfb_A1.sendTransform(ts_A1);
    tfb_A2.sendTransform(ts_A2);
    tfb_A3.sendTransform(ts_A3);
    tfb_A4.sendTransform(ts_A4);
    tfb_A5.sendTransform(ts_A5);
    tfb_A6.sendTransform(ts_A6);
    tfb_A7.sendTransform(ts_A7);
    tfb_A8.sendTransform(ts_A8);

    tfb_B1.sendTransform(ts_B1);
    tfb_B2.sendTransform(ts_B2);
    tfb_B3.sendTransform(ts_B3);
    tfb_B4.sendTransform(ts_B4);
    tfb_B5.sendTransform(ts_B5);
    tfb_B6.sendTransform(ts_B6);
    tfb_B7.sendTransform(ts_B7);
    tfb_B8.sendTransform(ts_B8);

    tfb_C1.sendTransform(ts_C1);
    tfb_C2.sendTransform(ts_C2);
    tfb_C3.sendTransform(ts_C3);
    tfb_C4.sendTransform(ts_C4);
    tfb_C5.sendTransform(ts_C5);
    tfb_C6.sendTransform(ts_C6);
    tfb_C7.sendTransform(ts_C7);
    tfb_C8.sendTransform(ts_C8);

    tfb_D1.sendTransform(ts_D1);
    tfb_D2.sendTransform(ts_D2);
    tfb_D3.sendTransform(ts_D3);
    tfb_D4.sendTransform(ts_D4);
    tfb_D5.sendTransform(ts_D5);
    tfb_D6.sendTransform(ts_D6);
    tfb_D7.sendTransform(ts_D7);
    tfb_D8.sendTransform(ts_D8);

    tfb_E1.sendTransform(ts_E1);
    tfb_E2.sendTransform(ts_E2);
    tfb_E3.sendTransform(ts_E3);
    tfb_E4.sendTransform(ts_E4);
    tfb_E5.sendTransform(ts_E5);
    tfb_E6.sendTransform(ts_E6);
    tfb_E7.sendTransform(ts_E7);
    tfb_E8.sendTransform(ts_E8);

    tfb_F1.sendTransform(ts_F1);
    tfb_F2.sendTransform(ts_F2);
    tfb_F3.sendTransform(ts_F3);
    tfb_F4.sendTransform(ts_F4);
    tfb_F5.sendTransform(ts_F5);
    tfb_F6.sendTransform(ts_F6);
    tfb_F7.sendTransform(ts_F7);
    tfb_F8.sendTransform(ts_F8);

    tfb_G1.sendTransform(ts_G1);
    tfb_G2.sendTransform(ts_G2);
    tfb_G3.sendTransform(ts_G3);
    tfb_G4.sendTransform(ts_G4);
    tfb_G5.sendTransform(ts_G5);
    tfb_G6.sendTransform(ts_G6);
    tfb_G7.sendTransform(ts_G7);
    tfb_G8.sendTransform(ts_G8);

    tfb_H1.sendTransform(ts_H1);
    tfb_H2.sendTransform(ts_H2);
    tfb_H3.sendTransform(ts_H3);
    tfb_H4.sendTransform(ts_H4);
    tfb_H5.sendTransform(ts_H5);
    tfb_H6.sendTransform(ts_H6);
    tfb_H7.sendTransform(ts_H7);
    tfb_H8.sendTransform(ts_H8);



    if (ts_aruco_100.transform.translation.z !=0 || ts_aruco_101.transform.translation.z !=0 || ts_aruco_102.transform.translation.z !=0 || ts_aruco_101.transform.translation.z !=0){
	calibration_needed = true;
}

    else if ((-0.216-bound < ts_aruco_100.transform.translation.x < -0.216+bound) || (-0.216-bound < ts_aruco_101.transform.translation.x < -0.216+bound) || (0.216-bound < ts_aruco_102.transform.translation.x < 0.216+bound) || (0.216-bound < ts_aruco_103.transform.translation.x < 0.216+bound)){
	calibration_needed = true;
}

    else if ((-0.216-bound < ts_aruco_100.transform.translation.y < -0.216+bound) || (0.216-bound < ts_aruco_101.transform.translation.y < 0.216+bound) || (-0.216-bound < ts_aruco_102.transform.translation.y < -0.216+bound) || (0.216-bound < ts_aruco_103.transform.translation.y < 0.216+bound)){
	calibration_needed = true;
}

    else{
	calibration_needed = false;
}

    
    rate.sleep();

    ros::spinOnce();
  }
  return 0;
};
