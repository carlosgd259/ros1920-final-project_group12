**ros1920-final-project**

This metapackage contains the repositories for the development of the final project of the course [Introduction to ROS](https://sir.upc.edu/projects/rostutorials) (*ETSEIB-Universitat Politècnica de Catalunya*)

* **chesslab_setup**: Project scene setup and seerver for basic planning services
* **ff**: ROS wrapper to the Fast Forward Task Planner
* **UR3-IK**: Inverse Kinematics of the UR3 robot
* **realsense-ros**: ROS Wrapper for Intel RealSense Devices
* **realsense_lab**: Realsense extrinsic parameters calibration
* **aruco_ros**: Software package and ROS wrappers of the Aruco Augmented Reality marker detector library (forked from pal-robotics/aruco_ros)
* **universal_robot**: This repository provides ROS support for the universal robots (forked from ros-industrial/universal_robot)
* **Universal_Robots_ROS_Driver**: Driver enabling ROS operation of UR robots. 
* **robotiq**: ROS-Industrial robotiq meta-package (forked from ros-industrial/robotiq)


[Final Work Webpage](https://sir.upc.edu/projects/rostutorials/final_work)