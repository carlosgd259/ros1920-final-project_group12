#include <chesslab_setup/attachobs2robot.h>
#include <chesslab_setup/dettachobs.h>
#include <chesslab_setup/ik.h>
#include <chesslab_setup/setobjpose.h>
#include <chesslab_setup/setrobconf.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <iostream>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <string>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <tf/tf.h>

using namespace std;

//#include <A1cell.h>
int a = 0;            //a is the variable used to select the piece.

bool checkpositions = false;
float posxA1= 00.0;
float posyA1= 00.0;
float posxA2= 00.0;
float posyA2= 00.0;
float posxA3= 00.0;
float posyA3= 00.0;
float posxA4= 00.0;
float posyA4= 00.0;
float posxA5= 00.0;
float posyA5= 00.0;
float posxA6= 00.0;
float posyA6= 00.0;
float posxA7= 00.0;
float posyA7= 00.0;
float posxA8= 00.0;
float posyA8= 00.0;

float posxB1= 00.0;
float posyB1= 00.0;
float posxB2= 00.0;
float posyB2= 00.0;
float posxB3= 00.0;
float posyB3= 00.0;
float posxB4= 00.0;
float posyB4= 00.0;
float posxB5= 00.0;
float posyB5= 00.0;
float posxB6= 00.0;
float posyB6= 00.0;
float posxB7= 00.0;
float posyB7= 00.0;
float posxB8= 00.0;
float posyB8= 00.0;

float posxC1= 00.0;
float posyC1= 00.0;
float posxC2= 00.0;
float posyC2= 00.0;
float posxC3= 00.0;
float posyC3= 00.0;
float posxC4= 00.0;
float posyC4= 00.0;
float posxC5= 00.0;
float posyC5= 00.0;
float posxC6= 00.0;
float posyC6= 00.0;
float posxC7= 00.0;
float posyC7= 00.0;
float posxC8= 00.0;
float posyC8= 00.0;

float posxD1= 00.0;
float posyD1= 00.0;
float posxD2= 00.0;
float posyD2= 00.0;
float posxD3= 00.0;
float posyD3= 00.0;
float posxD4= 00.0;
float posyD4= 00.0;
float posxD5= 00.0;
float posyD5= 00.0;
float posxD6= 00.0;
float posyD6= 00.0;
float posxD7= 00.0;
float posyD7= 00.0;
float posxD8= 00.0;
float posyD8= 00.0;

float posxE1= 00.0;
float posyE1= 00.0;
float posxE2= 00.0;
float posyE2= 00.0;
float posxE3= 00.0;
float posyE3= 00.0;
float posxE4= 00.0;
float posyE4= 00.0;
float posxE5= 00.0;
float posyE5= 00.0;
float posxE6= 00.0;
float posyE6= 00.0;
float posxE7= 00.0;
float posyE7= 00.0;
float posxE8= 00.0;
float posyE8= 00.0;

float posxF1= 00.0;
float posyF1= 00.0;
float posxF2= 00.0;
float posyF2= 00.0;
float posxF3= 00.0;
float posyF3= 00.0;
float posxF4= 00.0;
float posyF4= 00.0;
float posxF5= 00.0;
float posyF5= 00.0;
float posxF6= 00.0;
float posyF6= 00.0;
float posxF7= 00.0;
float posyF7= 00.0;
float posxF8= 00.0;
float posyF8= 00.0;

float posxG1= 00.0;
float posyG1= 00.0;
float posxG2= 00.0;
float posyG2= 00.0;
float posxG3= 00.0;
float posyG3= 00.0;
float posxG4= 00.0;
float posyG4= 00.0;
float posxG5= 00.0;
float posyG5= 00.0;
float posxG6= 00.0;
float posyG6= 00.0;
float posxG7= 00.0;
float posyG7= 00.0;
float posxG8= 00.0;
float posyG8= 00.0;

float posxH1= 00.0;
float posyH1= 00.0;
float posxH2= 00.0;
float posyH2= 00.0;
float posxH3= 00.0;
float posyH3= 00.0;
float posxH4= 00.0;
float posyH4= 00.0;
float posxH5= 00.0;
float posyH5= 00.0;
float posxH6= 00.0;
float posyH6= 00.0;
float posxH7= 00.0;
float posyH7= 00.0;
float posxH8= 00.0;
float posyH8= 00.0;

float posx201= 00.0;
float posy201= 00.0;
float posx202= 00.0;
float posy202= 00.0;
float posx203= 00.0;
float posy203= 00.0;
float posx204= 00.0;
float posy204= 00.0;
float posx205= 00.0;
float posy205= 00.0;
float posx206= 00.0;
float posy206= 00.0;
float posx207= 00.0;
float posy207= 00.0;
float posx208= 00.0;
float posy208= 00.0;

float posx209= 00.0;
float posy209= 00.0;
float posx210= 00.0;
float posy210= 00.0;
float posx211= 00.0;
float posy211= 00.0;
float posx212= 00.0;
float posy212= 00.0;
float posx213= 00.0;
float posy213= 00.0;
float posx214= 00.0;
float posy214= 00.0;
float posx215= 00.0;
float posy215= 00.0;
float posx216= 00.0;
float posy216= 00.0;


float posx301= 00.0;
float posy301= 00.0;
float posx302= 00.0;
float posy302= 00.0;
float posx303= 00.0;
float posy303= 00.0;
float posx304= 00.0;
float posy304= 00.0;
float posx305= 00.0;
float posy305= 00.0;
float posx306= 00.0;
float posy306= 00.0;
float posx307= 00.0;
float posy307= 00.0;
float posx308= 00.0;
float posy308= 00.0;

float posx309= 00.0;
float posy309= 00.0;
float posx310= 00.0;
float posy310= 00.0;
float posx311= 00.0;
float posy311= 00.0;
float posx312= 00.0;
float posy312= 00.0;
float posx313= 00.0;
float posy313= 00.0;
float posx314= 00.0;
float posy314= 00.0;
float posx315= 00.0;
float posy315= 00.0;
float posx316= 00.0;
float posy316= 00.0;

int arucos() {

	int iposxA1 = 100*posxA1; 
	int iposyA1 = 100*posyA1; 
	int iposxA2 = 100*posxA2; 
	int iposyA2 = 100*posyA2; 
	int iposxA3 = 100*posxA3; 
	int iposyA3 = 100*posyA3; 
	int iposxA4 = 100*posxA4; 
	int iposyA4 = 100*posyA4;
	int iposxA5 = 100*posxA5; 
	int iposyA5 = 100*posyA5; 
	int iposxA6 = 100*posxA6; 
	int iposyA6 = 100*posyA6; 
	int iposxA7 = 100*posxA7; 
	int iposyA7 = 100*posyA7; 
	int iposxA8 = 100*posxA8; 
	int iposyA8 = 100*posyA8;  

	int iposxB1 = 100*posxB1; 
	int iposyB1 = 100*posyB1; 
	int iposxB2 = 100*posxB2; 
	int iposyB2 = 100*posyB2; 
	int iposxB3 = 100*posxB3; 
	int iposyB3 = 100*posyB3; 
	int iposxB4 = 100*posxB4; 
	int iposyB4 = 100*posyB4;
	int iposxB5 = 100*posxB5; 
	int iposyB5 = 100*posyB5; 
	int iposxB6 = 100*posxB6; 
	int iposyB6 = 100*posyB6; 
	int iposxB7 = 100*posxB7; 
	int iposyB7 = 100*posyB7; 
	int iposxB8 = 100*posxB8; 
	int iposyB8 = 100*posyB8;

	int iposxC1 = 100*posxC1; 
	int iposyC1 = 100*posyC1; 
	int iposxC2 = 100*posxC2; 
	int iposyC2 = 100*posyC2; 
	int iposxC3 = 100*posxC3; 
	int iposyC3 = 100*posyC3; 
	int iposxC4 = 100*posxC4; 
	int iposyC4 = 100*posyC4;
	int iposxC5 = 100*posxC5; 
	int iposyC5 = 100*posyC5; 
	int iposxC6 = 100*posxC6; 
	int iposyC6 = 100*posyC6; 
	int iposxC7 = 100*posxC7; 
	int iposyC7 = 100*posyC7; 
	int iposxC8 = 100*posxC8; 
	int iposyC8 = 100*posyC8;  

	int iposxD1 = 100*posxD1; 
	int iposyD1 = 100*posyD1; 
	int iposxD2 = 100*posxD2; 
	int iposyD2 = 100*posyD2; 
	int iposxD3 = 100*posxD3; 
	int iposyD3 = 100*posyD3; 
	int iposxD4 = 100*posxD4; 
	int iposyD4 = 100*posyD4;
	int iposxD5 = 100*posxD5; 
	int iposyD5 = 100*posyD5; 
	int iposxD6 = 100*posxD6; 
	int iposyD6 = 100*posyD6; 
	int iposxD7 = 100*posxD7; 
	int iposyD7 = 100*posyD7; 
	int iposxD8 = 100*posxD8; 
	int iposyD8 = 100*posyD8;

	int iposxE1 = 100*posxE1; 
	int iposyE1 = 100*posyE1; 
	int iposxE2 = 100*posxE2; 
	int iposyE2 = 100*posyE2; 
	int iposxE3 = 100*posxE3; 
	int iposyE3 = 100*posyE3; 
	int iposxE4 = 100*posxE4; 
	int iposyE4 = 100*posyE4;
	int iposxE5 = 100*posxE5; 
	int iposyE5 = 100*posyE5; 
	int iposxE6 = 100*posxE6; 
	int iposyE6 = 100*posyE6; 
	int iposxE7 = 100*posxE7; 
	int iposyE7 = 100*posyE7; 
	int iposxE8 = 100*posxE8; 
	int iposyE8 = 100*posyE8;  

	int iposxF1 = 100*posxF1; 
	int iposyF1 = 100*posyF1; 
	int iposxF2 = 100*posxF2; 
	int iposyF2 = 100*posyF2; 
	int iposxF3 = 100*posxF3; 
	int iposyF3 = 100*posyF3; 
	int iposxF4 = 100*posxF4; 
	int iposyF4 = 100*posyF4;
	int iposxF5 = 100*posxF5; 
	int iposyF5 = 100*posyF5; 
	int iposxF6 = 100*posxF6; 
	int iposyF6 = 100*posyF6; 
	int iposxF7 = 100*posxF7; 
	int iposyF7 = 100*posyF7; 
	int iposxF8 = 100*posxF8; 
	int iposyF8 = 100*posyF8;

	int iposxG1 = 100*posxG1; 
	int iposyG1 = 100*posyG1; 
	int iposxG2 = 100*posxG2; 
	int iposyG2 = 100*posyG2; 
	int iposxG3 = 100*posxG3; 
	int iposyG3 = 100*posyG3; 
	int iposxG4 = 100*posxG4; 
	int iposyG4 = 100*posyG4;
	int iposxG5 = 100*posxG5; 
	int iposyG5 = 100*posyG5; 
	int iposxG6 = 100*posxG6; 
	int iposyG6 = 100*posyG6; 
	int iposxG7 = 100*posxG7; 
	int iposyG7 = 100*posyG7; 
	int iposxG8 = 100*posxG8; 
	int iposyG8 = 100*posyG8;  

	int iposxH1 = 100*posxH1; 
	int iposyH1 = 100*posyH1; 
	int iposxH2 = 100*posxH2; 
	int iposyH2 = 100*posyH2; 
	int iposxH3 = 100*posxH3; 
	int iposyH3 = 100*posyH3; 
	int iposxH4 = 100*posxH4; 
	int iposyH4 = 100*posyH4;
	int iposxH5 = 100*posxH5; 
	int iposyH5 = 100*posyH5; 
	int iposxH6 = 100*posxH6; 
	int iposyH6 = 100*posyH6; 
	int iposxH7 = 100*posxH7; 
	int iposyH7 = 100*posyH7; 
	int iposxH8 = 100*posxH8; 
	int iposyH8 = 100*posyH8;

	int iposx201 = 100*posx201; 
	int iposy201 = 100*posy201; 
	int iposx202 = 100*posx202; 
	int iposy202 = 100*posy202; 
	int iposx203 = 100*posx203; 
	int iposy203 = 100*posy203; 
	int iposx204 = 100*posx204; 
	int iposy204 = 100*posy204; 
	int iposx205 = 100*posx205; 
	int iposy205 = 100*posy205; 
	int iposx206 = 100*posx206; 
	int iposy206 = 100*posy206; 
	int iposx207 = 100*posx207; 
	int iposy207 = 100*posy207; 
	int iposx208 = 100*posx208; 
	int iposy208 = 100*posy208; 
	int iposx209 = 100*posx209; 
	int iposy209 = 100*posy209; 
	int iposx210 = 100*posx210; 
	int iposy210 = 100*posy210; 
	int iposx211 = 100*posx211; 
	int iposy211 = 100*posy211; 
	int iposx212 = 100*posx212; 
	int iposy212 = 100*posy212; 
	int iposx213 = 100*posx213; 
	int iposy213 = 100*posy213; 
	int iposx214 = 100*posx214; 
	int iposy214 = 100*posy214; 
	int iposx215 = 100*posx215; 
	int iposy215 = 100*posy215; 
	int iposx216 = 100*posx216; 
	int iposy216 = 100*posy216;

	int iposx301 = 100*posx301; 
	int iposy301 = 100*posy301; 
	int iposx302 = 100*posx302; 
	int iposy302 = 100*posy302; 
	int iposx303 = 100*posx303; 
	int iposy303 = 100*posy303; 
	int iposx304 = 100*posx304; 
	int iposy304 = 100*posy304; 
	int iposx305 = 100*posx305; 
	int iposy305 = 100*posy305; 
	int iposx306 = 100*posx306; 
	int iposy306 = 100*posy306; 
	int iposx307 = 100*posx307; 
	int iposy307 = 100*posy307; 
	int iposx308 = 100*posx308; 
	int iposy308 = 100*posy308; 
	int iposx309 = 100*posx309; 
	int iposy309 = 100*posy309; 
	int iposx310 = 100*posx310; 
	int iposy310 = 100*posy310; 
	int iposx311 = 100*posx311; 
	int iposy311 = 100*posy311; 
	int iposx312 = 100*posx312; 
	int iposy312 = 100*posy312; 
	int iposx313 = 100*posx313; 
	int iposy313 = 100*posy313; 
	int iposx314 = 100*posx314; 
	int iposy314 = 100*posy314; 
	int iposx315 = 100*posx315; 
	int iposy315 = 100*posy315; 
	int iposx316 = 100*posx316; 
	int iposy316 = 100*posy316;

	int ixposes[] = { iposyA1, iposyA2, iposyA3, iposyA4, iposyA5, iposyA6, iposyA7, iposyA8, iposyB1, iposyB2, iposyB3, iposyB4, iposyB5, iposyB6, iposyB7, iposyB8, iposyC1, iposyC2, iposyC3, iposyC4, iposyC5, iposyC6, iposyC7, iposyC8, iposyD1, iposyD2, iposyD3, iposyD4, iposyD5, iposyD6, iposyD7, iposyD8, iposyE1, iposyE2, iposyE3, iposyE4, iposyE5, iposyE6, iposyE7, iposyE8, iposyF1, iposyF2, iposyF3, iposyF4, iposyF5, iposyF6, iposyF7, iposyF8, iposyG1, iposyG2, iposyG3, iposyG4, iposyG5, iposyG6, iposyG7, iposyG8, iposyH1, iposyH2, iposyH3, iposyH4, iposyH5, iposyH6, iposyH7, iposyH8};

	int iyposes[] = { iposxA1, iposxA2, iposxA3, iposxA4, iposxA5, iposxA6, iposxA7, iposxA8, iposxB1, iposxB2, iposxB3, iposxB4, iposxB5, iposxB6, iposxB7, iposxB8, iposxC1, iposxC2, iposxC3, iposxC4, iposxC5, iposxC6, iposxC7, iposxC8, iposxD1, iposxD2, iposxD3, iposxD4, iposxD5, iposxD6, iposxD7, iposxD8, iposxE1, iposxE2, iposxE3, iposxE4, iposxE5, iposxE6, iposxE7, iposxE8, iposxF1, iposxF2, iposxF3, iposxF4, iposxF5, iposxF6, iposxF7, iposxF8, iposxG1, iposxG2, iposxG3, iposxG4, iposxG5, iposxG6, iposxG7, iposxG8, iposxH1, iposxH2, iposxH3, iposxH4, iposxH5, iposxH6, iposxH7, iposxH8};

	int arucoxposes[] = { iposx201, iposx202, iposx203, iposx204, iposx205, iposx206, iposx207, iposx208, iposx209, iposx210, iposx211, iposx212, iposx213, iposx214, iposx215, iposx216, iposx301, iposx302, iposx303, iposx304, iposx305, iposx306, iposx307, iposx308, iposx309, iposx310, iposx311, iposx312, iposx313, iposx314, iposx315, iposx316};

	int arucoyposes[] = { iposy201, iposy202, iposy203, iposy204, iposy205, iposy206, iposy207, iposy208, iposy209, iposy210, iposy211, iposy212, iposy213, iposy214, iposy215, iposy216, iposy301, iposy302, iposy303, iposy304, iposy305, iposy306, iposy307, iposy308, iposy309, iposy310, iposy311, iposy312, iposy313, iposy314, iposy315, iposy316};

	int* iposes[] = {ixposes, iyposes};
	int* iarucos[] = {arucoxposes,arucoyposes};
	const char* cellnames[64] = {"A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8"};

	std::string chessbox;
	std::cout << "please, the pice desired is:";
	std::getline(std::cin,chessbox);

	if (chessbox =="201"){
		a = 0;
	}
	if (chessbox =="202"){
		a = 1;
	}
	if (chessbox =="203"){
		a = 2;
	}
	if (chessbox =="204"){
		a = 3;
	}
	if (chessbox =="205"){
		a = 4;
	}
	if (chessbox =="206"){
		a = 5;
	}
	if (chessbox =="207"){
		a = 6;
	}
	if (chessbox =="208"){
		a = 7;
	}
	if (chessbox =="209"){
		a = 8;
	}
	if (chessbox =="210"){
		a = 9;
	}
	if (chessbox =="211"){
		a = 10;
	}
	if (chessbox =="212"){
		a = 11;
	}
	if (chessbox =="213"){
		a = 12;
	}
	if (chessbox =="214"){
		a = 13;
	}
	if (chessbox =="215"){
		a = 14;
	}
	if (chessbox =="216"){
		a = 15;
	}

	if (chessbox =="301"){
		a = 16;
	}
	if (chessbox =="302"){
		a = 17;
	}
	if (chessbox =="303"){
		a = 18;
	}
	if (chessbox =="304"){
		a = 19;
	}
	if (chessbox =="305"){
		a = 20;
	}
	if (chessbox =="306"){
		a = 21;
	}
	if (chessbox =="307"){
		a = 22;
	}
	if (chessbox =="308"){
		a = 23;
	}
	if (chessbox =="309"){
		a = 24;
	}
	if (chessbox =="310"){
		a = 25;
	}
	if (chessbox =="311"){
		a = 26;
	}
	if (chessbox =="312"){
		a = 27;
	}
	if (chessbox =="313"){
		a = 28;
	}
	if (chessbox =="314"){
		a = 29;
	}
	if (chessbox =="315"){
		a = 30;
	}
	if (chessbox =="316"){
		a = 31;
	}


	for (int j = 0; j<64; j++){

		if (((sqrt((iposes[0][j] -iarucos[0][a])*(iposes[0][j] -iarucos[0][a])))<=1) and ((sqrt((iposes[1][j] -iarucos[1][a])*(iposes[1][j] -iarucos[1][a])))<=1)){
			ROS_INFO_STREAM("The cell found is:"<< " "<< cellnames[j]);
		}
	}

}

void robot_pose(float x,float y,float z,float orient_x,float orient_y,float orient_z,float orient_w,float gripper){

	ros::NodeHandle node;

	//call the inversekin service
	ros::service::waitForService("/chesslab_setup/inversekin");
	ros::ServiceClient inversekin_client = node.serviceClient<chesslab_setup::ik>("/chesslab_setup/inversekin");
	chesslab_setup::ik inversekin_srv;

	ros::service::waitForService("/chesslab_setup/setrobconf");
	ros::ServiceClient setrobconf_client = node.serviceClient<chesslab_setup::setrobconf>("/chesslab_setup/setrobconf");
	chesslab_setup::setrobconf setrobconf_srv;

	inversekin_srv.request.pose.position.x = y;
	inversekin_srv.request.pose.position.y = x;
	inversekin_srv.request.pose.position.z = z;
	inversekin_srv.request.pose.orientation.x = orient_x;
	inversekin_srv.request.pose.orientation.y = orient_y;
	inversekin_srv.request.pose.orientation.z = orient_z;
	inversekin_srv.request.pose.orientation.w = orient_w;

	inversekin_client.call(inversekin_srv);

	std::stringstream sstr;
	if(inversekin_srv.response.status){
		sstr<<"The computed ik is:"<<std::endl;
		for(int i=0; i<inversekin_srv.response.ik_solution.size(); i++){
			sstr << "[";
			for(int j=0; j<5; j++){
				sstr << inversekin_srv.response.ik_solution[i].ik[j] <<", ";            
			}
			sstr << inversekin_srv.response.ik_solution[i].ik[5] << "]" << std::endl;
		}
	}
	else{
		ROS_INFO("Not able to compute the ik");
	}

	setrobconf_srv.request.conf.resize(14);
	setrobconf_srv.request.conf[0]  =  inversekin_srv.response.ik_solution[0].ik[0];
	setrobconf_srv.request.conf[1]  =  inversekin_srv.response.ik_solution[0].ik[1];
	setrobconf_srv.request.conf[2]  =  inversekin_srv.response.ik_solution[0].ik[2];
	setrobconf_srv.request.conf[3]  =  inversekin_srv.response.ik_solution[0].ik[3];
	setrobconf_srv.request.conf[4]  =  inversekin_srv.response.ik_solution[0].ik[4];
	setrobconf_srv.request.conf[5]  =  inversekin_srv.response.ik_solution[0].ik[5];
	setrobconf_srv.request.conf[6]  =  gripper;
	setrobconf_srv.request.conf[7]  =  0.0;
	setrobconf_srv.request.conf[8]  =  0.0;
	setrobconf_srv.request.conf[9]  =  0.0;
	setrobconf_srv.request.conf[10] =  0.0;
	setrobconf_srv.request.conf[11] =  0.0;
	setrobconf_srv.request.conf[12] =  0.0;
	setrobconf_srv.request.conf[13] =  0.0;

	setrobconf_client.call(setrobconf_srv);

	if(setrobconf_srv.response.incollision == true){
		std::cout << "The configuration is not collision-free"<< std::endl;
		std::cout << setrobconf_srv.response.msg;
		std::cout << "The collided obstacle aruco ID is " << setrobconf_srv.response.obj << std::endl;
	}
	else{
		std::cout << setrobconf_srv.response.msg;
	}
}




bool servtest1(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){
	arucos();
	return true;
}

bool servtest2(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	float xposes[] = { posyA1, posyA2, posyA3, posyA4, posyA5, posyA6, posyA7, posyA8, posyB1, posyB2, posyB3, posyB4, posyB5, posyB6, posyB7, posyB8, posyC1, posyC2, posyC3, posyC4, posyC5, posyC6, posyC7, posyC8, posyD1, posyD2, posyD3, posyD4, posyD5, posyD6, posyD7, posyD8, posyE1, posyE2, posyE3, posyE4, posyE5, posyE6, posyE7, posyE8, posyF1, posyF2, posyF3, posyF4, posyF5, posyF6, posyF7, posyF8, posyG1, posyG2, posyG3, posyG4, posyG5, posyG6, posyG7, posyG8, posyH1, posyH2, posyH3, posyH4, posyH5, posyH6, posyH7, posyH8};

	float yposes[] = { posxA1, posxA2, posxA3, posxA4, posxA5, posxA6, posxA7, posxA8, posxB1, posxB2, posxB3, posxB4, posxB5, posxB6, posxB7, posxB8, posxC1, posxC2, posxC3, posxC4, posxC5, posxC6, posxC7, posxC8, posxD1, posxD2, posxD3, posxD4, posxD5, posxD6, posxD7, posxD8, posxE1, posxE2, posxE3, posxE4, posxE5, posxE6, posxE7, posxE8, posxF1, posxF2, posxF3, posxF4, posxF5, posxF6, posxF7, posxF8, posxG1, posxG2, posxG3, posxG4, posxG5, posxG6, posxG7, posxG8, posxH1, posxH2, posxH3, posxH4, posxH5, posxH6, posxH7, posxH8};

	float* poses[] = {xposes, yposes};

	std::string chessbox;
	std::string chessbox1;
	std::string chessbox2;
	float x_cell;
	float y_cell;

	std::cout << "Please select the cell where move the robot: ";
	std::getline (std::cin,chessbox);

	if (chessbox == "A1") {
		y_cell = -poses[0][0];
		x_cell = poses[1][0]+0.37;
	}
	if (chessbox == "A2") {
		y_cell = -poses[0][1];
		x_cell = poses[1][1]+0.37;
	}
	if (chessbox == "A3") {
		y_cell = -poses[0][2];
		x_cell = poses[1][2]+0.37;
	}
	if (chessbox == "A4") {
		y_cell = -poses[0][3];
		x_cell = poses[1][3]+0.37;
	}
	if (chessbox == "A5") {
		y_cell = -poses[0][4];
		x_cell = poses[1][4]+0.37;
	}
	if (chessbox == "A6") {
		y_cell = -poses[0][5];
		x_cell = poses[1][5]+0.37;
	}
	if (chessbox == "A7") {
		y_cell = -poses[0][6];
		x_cell = poses[1][6]+0.37;
	}
	if (chessbox == "A8") {
		y_cell = -poses[0][7];
		x_cell = poses[1][7]+0.37;
	}

	if (chessbox == "B1") {
		y_cell = -poses[0][8];
		x_cell = poses[1][8]+0.37;
	}
	if (chessbox == "B2") {
		y_cell = -poses[0][9];
		x_cell = poses[1][9]+0.37;
	}
	if (chessbox == "B3") {
		y_cell = -poses[0][10];
		x_cell = poses[1][10]+0.37;
	}
	if (chessbox == "B4") {
		y_cell = -poses[0][11];
		x_cell = poses[1][11]+0.37;
	}
	if (chessbox == "B5") {
		y_cell = -poses[0][12];
		x_cell = poses[1][12]+0.37;
	}
	if (chessbox == "B6") {
		y_cell = -poses[0][13];
		x_cell = poses[1][13]+0.37;
	}
	if (chessbox == "B7") {
		y_cell = -poses[0][14];
		x_cell = poses[1][14]+0.37;
	}
	if (chessbox == "B8") {
		y_cell = -poses[0][15];
		x_cell = poses[1][15]+0.37;
	}

	if (chessbox == "C1") {
		y_cell = -poses[0][16];
		x_cell = poses[1][16]+0.37;
	}
	if (chessbox == "C2") {
		y_cell = -poses[0][17];
		x_cell = poses[1][17]+0.37;
	}
	if (chessbox == "C3") {
		y_cell = -poses[0][18];
		x_cell = poses[1][18]+0.37;
	}
	if (chessbox == "C4") {
		y_cell = -poses[0][19];
		x_cell = poses[1][19]+0.37;
	}
	if (chessbox == "C5") {
		y_cell = -poses[0][20];
		x_cell = poses[1][20]+0.37;
	}
	if (chessbox == "C6") {
		y_cell = -poses[0][21];
		x_cell = poses[1][21]+0.37;
	}
	if (chessbox == "C7") {
		y_cell = -poses[0][22];
		x_cell = poses[1][22]+0.37;
	}
	if (chessbox == "C8") {
		y_cell = -poses[0][23];
		x_cell = poses[1][23]+0.37;
	}

	if (chessbox == "D1") {
		y_cell = -poses[0][24];
		x_cell = poses[1][24]+0.37;
	}
	if (chessbox == "D2") {
		y_cell = -poses[0][25];
		x_cell = poses[1][25]+0.37;
	}
	if (chessbox == "D3") {
		y_cell = -poses[0][26];
		x_cell = poses[1][26]+0.37;
	}
	if (chessbox == "D4") {
		y_cell = -poses[0][27];
		x_cell = poses[1][27]+0.37;
	}
	if (chessbox == "D5") {
		y_cell = -poses[0][28];
		x_cell = poses[1][28]+0.37;
	}
	if (chessbox == "D6") {
		y_cell = -poses[0][29];
		x_cell = poses[1][29]+0.37;
	}
	if (chessbox == "D7") {
		y_cell = -poses[0][30];
		x_cell = poses[1][30]+0.37;
	}
	if (chessbox == "D8") {
		y_cell = -poses[0][31];
		x_cell = poses[1][31]+0.37;
	}

	if (chessbox == "E1") {
		y_cell = -poses[0][32];
		x_cell = poses[1][32]+0.37;
	}
	if (chessbox == "E2") {
		y_cell = -poses[0][33];
		x_cell = poses[1][33]+0.37;
	}
	if (chessbox == "E3") {
		y_cell = -poses[0][34];
		x_cell = poses[1][34]+0.37;
	}
	if (chessbox == "E4") {
		y_cell = -poses[0][35];
		x_cell = poses[1][35]+0.37;
	}
	if (chessbox == "E5") {
		y_cell = -poses[0][36];
		x_cell = poses[1][36]+0.37;
	}
	if (chessbox == "E6") {
		y_cell = -poses[0][37];
		x_cell = poses[1][37]+0.37;
	}
	if (chessbox == "E7") {
		y_cell = -poses[0][38];
		x_cell = poses[1][38]+0.37;
	}
	if (chessbox == "E8") {
		y_cell = -poses[0][39];
		x_cell = poses[1][39]+0.37;
	}

	if (chessbox == "F1") {
		y_cell = -poses[0][40];
		x_cell = poses[1][40]+0.37;
	}
	if (chessbox == "F2") {
		y_cell = -poses[0][41];
		x_cell = poses[1][41]+0.37;
	}
	if (chessbox == "F3") {
		y_cell = -poses[0][42];
		x_cell = poses[1][42]+0.37;
	}
	if (chessbox == "F4") {
		y_cell = -poses[0][43];
		x_cell = poses[1][43]+0.37;
	}
	if (chessbox == "F5") {
		y_cell = -poses[0][44];
		x_cell = poses[1][44]+0.37;
	}
	if (chessbox == "F6") {
		y_cell = -poses[0][45];
		x_cell = poses[1][45]+0.37;
	}
	if (chessbox == "F7") {
		y_cell = -poses[0][46];
		x_cell = poses[1][46]+0.37;
	}
	if (chessbox == "F8") {
		y_cell = -poses[0][47];
		x_cell = poses[1][47]+0.37;
	}

	if (chessbox == "G1") {
		y_cell = -poses[0][48];
		x_cell = poses[1][48]+0.37;
	}
	if (chessbox == "G2") {
		y_cell = -poses[0][49];
		x_cell = poses[1][49]+0.37;
	}
	if (chessbox == "G3") {
		y_cell = -poses[0][50];
		x_cell = poses[1][50]+0.37;
	}
	if (chessbox == "G4") {
		y_cell = -poses[0][51];
		x_cell = poses[1][51]+0.37;
	}
	if (chessbox == "G5") {
		y_cell = -poses[0][52];
		x_cell = poses[1][52]+0.37;
	}
	if (chessbox == "G6") {
		y_cell = -poses[0][53];
		x_cell = poses[1][53]+0.37;
	}
	if (chessbox == "G7") {
		y_cell = -poses[0][54];
		x_cell = poses[1][54]+0.37;
	}
	if (chessbox == "G8") {
		y_cell = -poses[0][55];
		x_cell = poses[1][55]+0.37;
	}

	if (chessbox == "H1") {
		y_cell = -poses[0][56];
		x_cell = poses[1][56]+0.37;
	}
	if (chessbox == "H2") {
		y_cell = -poses[0][57];
		x_cell = poses[1][57]+0.37;
	}
	if (chessbox == "H3") {
		y_cell = -poses[0][58];
		x_cell = poses[1][58]+0.37;
	}
	if (chessbox == "H4") {
		y_cell = -poses[0][59];
		x_cell = poses[1][59]+0.37;
	}
	if (chessbox == "H5") {
		y_cell = -poses[0][60];
		x_cell = poses[1][60]+0.37;
	}
	if (chessbox == "H6") {
		y_cell = -poses[0][61];
		x_cell = poses[1][61]+0.37;
	}
	if (chessbox == "H7") {
		y_cell = -poses[0][62];
		x_cell = poses[1][62]+0.37;
	}
	if (chessbox == "H8") {
		y_cell = -poses[0][63];
		x_cell = poses[1][63]+0.37;
	}

	ROS_INFO_STREAM("Moving the robot to the pre-grasp configuration of the desired cell" << x_cell << "===" << y_cell);

	robot_pose(0.25, 0.30, 0.25, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(0.25, 0.30, 0.30, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(0.25, 0.30, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);
	int steps=5;

	for (int i = 0; i < steps+1; i++) {
		std::cout << i;
		robot_pose(0.25+i*(x_cell-0.25)/steps, 0.30+i*(y_cell-0.30)/steps, 0.345, 0.707, -0.707, 0.0, 0.0, 0.59);
	}

	return true;
}


bool servtest3(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;
	ros::service::waitForService("/chesslab_setup/resetscene");
	ros::ServiceClient resetscene_client = node.serviceClient<std_srvs::Empty>("/chesslab_setup/resetscene");
	std_srvs::Empty resetscene_srv;
	resetscene_client.call(resetscene_srv);

	float xposes[] = { posyA1, posyA2, posyA3, posyA4, posyA5, posyA6, posyA7, posyA8, posyB1, posyB2, posyB3, posyB4, posyB5, posyB6, posyB7, posyB8, posyC1, posyC2, posyC3, posyC4, posyC5, posyC6, posyC7, posyC8, posyD1, posyD2, posyD3, posyD4, posyD5, posyD6, posyD7, posyD8, posyE1, posyE2, posyE3, posyE4, posyE5, posyE6, posyE7, posyE8, posyF1, posyF2, posyF3, posyF4, posyF5, posyF6, posyF7, posyF8, posyG1, posyG2, posyG3, posyG4, posyG5, posyG6, posyG7, posyG8, posyH1, posyH2, posyH3, posyH4, posyH5, posyH6, posyH7, posyH8};

	float yposes[] = { posxA1, posxA2, posxA3, posxA4, posxA5, posxA6, posxA7, posxA8, posxB1, posxB2, posxB3, posxB4, posxB5, posxB6, posxB7, posxB8, posxC1, posxC2, posxC3, posxC4, posxC5, posxC6, posxC7, posxC8, posxD1, posxD2, posxD3, posxD4, posxD5, posxD6, posxD7, posxD8, posxE1, posxE2, posxE3, posxE4, posxE5, posxE6, posxE7, posxE8, posxF1, posxF2, posxF3, posxF4, posxF5, posxF6, posxF7, posxF8, posxG1, posxG2, posxG3, posxG4, posxG5, posxG6, posxG7, posxG8, posxH1, posxH2, posxH3, posxH4, posxH5, posxH6, posxH7, posxH8};

	float* poses[] = {xposes, yposes};
 
	std::string chessbox;
	float x_cell;
	float y_cell;
	std::string piece;

	std::cout << "Please select the piece to be moved: ";
	std::getline (std::cin,piece);

	std::cout << "Please select the cell where move the robot: ";
	std::getline (std::cin,chessbox);

	if (chessbox == "A1") {
		y_cell = -poses[0][0];
		x_cell = poses[1][0]+0.37;
	}
	if (chessbox == "A2") {
		y_cell = -poses[0][1];
		x_cell = poses[1][1]+0.37;
	}
	if (chessbox == "A3") {
		y_cell = -poses[0][2];
		x_cell = poses[1][2]+0.37;
	}
	if (chessbox == "A4") {
		y_cell = -poses[0][3];
		x_cell = poses[1][3]+0.37;
	}
	if (chessbox == "A5") {
		y_cell = -poses[0][4];
		x_cell = poses[1][4]+0.37;
	}
	if (chessbox == "A6") {
		y_cell = -poses[0][5];
		x_cell = poses[1][5]+0.37;
	}
	if (chessbox == "A7") {
		y_cell = -poses[0][6];
		x_cell = poses[1][6]+0.37;
	}
	if (chessbox == "A8") {
		y_cell = -poses[0][7];
		x_cell = poses[1][7]+0.37;
	}
	if (chessbox == "B1") {
		y_cell = -poses[0][8];
		x_cell = poses[1][8]+0.37;
	}
	if (chessbox == "B2") {
		y_cell = -poses[0][9];
		x_cell = poses[1][9]+0.37;
	}
	if (chessbox == "B3") {
		y_cell = -poses[0][10];
		x_cell = poses[1][10]+0.37;
	}
	if (chessbox == "B4") {
		y_cell = -poses[0][11];
		x_cell = poses[1][11]+0.37;
	}
	if (chessbox == "B5") {
		y_cell = -poses[0][12];
		x_cell = poses[1][12]+0.37;
	}
	if (chessbox == "B6") {
		y_cell = -poses[0][13];
		x_cell = poses[1][13]+0.37;
	}
	if (chessbox == "B7") {
		y_cell = -poses[0][14];
		x_cell = poses[1][14]+0.37;
	}
	if (chessbox == "B8") {
		y_cell = -poses[0][15];
		x_cell = poses[1][15]+0.37;
	}
	if (chessbox == "C1") {
		y_cell = -poses[0][16];
		x_cell = poses[1][16]+0.37;
	}
	if (chessbox == "C2") {
		y_cell = -poses[0][17];
		x_cell = poses[1][17]+0.37;
	}
	if (chessbox == "C3") {
		y_cell = -poses[0][18];
		x_cell = poses[1][18]+0.37;
	}
	if (chessbox == "C4") {
		y_cell = -poses[0][19];
		x_cell = poses[1][19]+0.37;
	}
	if (chessbox == "C5") {
		y_cell = -poses[0][20];
		x_cell = poses[1][20]+0.37;
	}
	if (chessbox == "C6") {
		y_cell = -poses[0][21];
		x_cell = poses[1][21]+0.37;
	}
	if (chessbox == "C7") {
		y_cell = -poses[0][22];
		x_cell = poses[1][22]+0.37;
	}
	if (chessbox == "C8") {
		y_cell = -poses[0][23];
		x_cell = poses[1][23]+0.37;
	}
	if (chessbox == "D1") {
		y_cell = -poses[0][24];
		x_cell = poses[1][24]+0.37;
	}
	if (chessbox == "D2") {
		y_cell = -poses[0][25];
		x_cell = poses[1][25]+0.37;
	}
	if (chessbox == "D3") {
		y_cell = -poses[0][26];
		x_cell = poses[1][26]+0.37;
	}
	if (chessbox == "D4") {
		y_cell = -poses[0][27];
		x_cell = poses[1][27]+0.37;
	}
	if (chessbox == "D5") {
		y_cell = -poses[0][28];
		x_cell = poses[1][28]+0.37;
	}
	if (chessbox == "D6") {
		y_cell = -poses[0][29];
		x_cell = poses[1][29]+0.37;
	}
	if (chessbox == "D7") {
		y_cell = -poses[0][30];
		x_cell = poses[1][30]+0.37;
	}
	if (chessbox == "D8") {
		y_cell = -poses[0][31];
		x_cell = poses[1][31]+0.37;
	}
	if (chessbox == "E1") {
		y_cell = -poses[0][32];
		x_cell = poses[1][32]+0.37;
	}
	if (chessbox == "E2") {
		y_cell = -poses[0][33];
		x_cell = poses[1][33]+0.37;
	}
	if (chessbox == "E3") {
		y_cell = -poses[0][34];
		x_cell = poses[1][34]+0.37;
	}
	if (chessbox == "E4") {
		y_cell = -poses[0][35];
		x_cell = poses[1][35]+0.37;
	}
	if (chessbox == "E5") {
		y_cell = -poses[0][36];
		x_cell = poses[1][36]+0.37;
	}
	if (chessbox == "E6") {
		y_cell = -poses[0][37];
		x_cell = poses[1][37]+0.37;
	}
	if (chessbox == "E7") {
		y_cell = -poses[0][38];
		x_cell = poses[1][38]+0.37;
	}
	if (chessbox == "E8") {
		y_cell = -poses[0][39];
		x_cell = poses[1][39]+0.37;
	}
	if (chessbox == "F1") {
		y_cell = -poses[0][40];
		x_cell = poses[1][40]+0.37;
	}
	if (chessbox == "F2") {
		y_cell = -poses[0][41];
		x_cell = poses[1][41]+0.37;
	}
	if (chessbox == "F3") {
		y_cell = -poses[0][42];
		x_cell = poses[1][42]+0.37;
	}
	if (chessbox == "F4") {
		y_cell = -poses[0][43];
		x_cell = poses[1][43]+0.37;
	}
	if (chessbox == "F5") {
		y_cell = -poses[0][44];
		x_cell = poses[1][44]+0.37;
	}
	if (chessbox == "F6") {
		y_cell = -poses[0][45];
		x_cell = poses[1][45]+0.37;
	}
	if (chessbox == "F7") {
		y_cell = -poses[0][46];
		x_cell = poses[1][46]+0.37;
	}
	if (chessbox == "F8") {
		y_cell = -poses[0][47];
		x_cell = poses[1][47]+0.37;
	}
	if (chessbox == "G1") {
		y_cell = -poses[0][48];
		x_cell = poses[1][48]+0.37;
	}
	if (chessbox == "G2") {
		y_cell = -poses[0][49];
		x_cell = poses[1][49]+0.37;
	}
	if (chessbox == "G3") {
		y_cell = -poses[0][50];
		x_cell = poses[1][50]+0.37;
	}
	if (chessbox == "G4") {
		y_cell = -poses[0][51];
		x_cell = poses[1][51]+0.37;
	}
	if (chessbox == "G5") {
		y_cell = -poses[0][52];
		x_cell = poses[1][52]+0.37;
	}
	if (chessbox == "G6") {
		y_cell = -poses[0][53];
		x_cell = poses[1][53]+0.37;
	}
	if (chessbox == "G7") {
		y_cell = -poses[0][54];
		x_cell = poses[1][54]+0.37;
	}
	if (chessbox == "G8") {
		y_cell = -poses[0][55];
		x_cell = poses[1][55]+0.37;
	}
	if (chessbox == "H1") {
		y_cell = -poses[0][56];
		x_cell = poses[1][56]+0.37;
	}
	if (chessbox == "H2") {
		y_cell = -poses[0][57];
		x_cell = poses[1][57]+0.37;
	}
	if (chessbox == "H3") {
		y_cell = -poses[0][58];
		x_cell = poses[1][58]+0.37;
	}
	if (chessbox == "H4") {
		y_cell = -poses[0][59];
		x_cell = poses[1][59]+0.37;
	}
	if (chessbox == "H5") {
		y_cell = -poses[0][60];
		x_cell = poses[1][60]+0.37;
	}
	if (chessbox == "H6") {
		y_cell = -poses[0][61];
		x_cell = poses[1][61]+0.37;
	}
	if (chessbox == "H7") {
		y_cell = -poses[0][62];
		x_cell = poses[1][62]+0.37;
	}
	if (chessbox == "H8") {
		y_cell = -poses[0][63];
		x_cell = poses[1][63]+0.37;
	}

	//Move piece
	ros::service::waitForService("/chesslab_setup/setobjpose");
	ros::ServiceClient setobjpose_client = node.serviceClient<chesslab_setup::setobjpose>("/chesslab_setup/setobjpose");
	chesslab_setup::setobjpose setobjpose_srv;	

	ROS_INFO("Moving the robot to the pre-grasp configuration of the desired cell");

	float high1;
	float high2;
	float high3;

	if ((piece=="215")||(piece=="216")||(piece=="315")||(piece=="316"))
	{
		high1=0.0;
		high2=0.235;
		high3=0.36;
	}
	else if ((piece=="209")||(piece=="210")||(piece=="211")||(piece=="212")||(piece=="213")||(piece=="214")||(piece=="309")||(piece=="310")||(piece=="311")||(piece=="312")||(piece=="313")||(piece=="314"))
	{
		high1=-0.01;
		high2=0.215;
		high3=0.34;
	}
	else 
	{
		high1=-0.02;
		high2=0.195;
		high3=0.32;
	}

	setobjpose_srv.request.objid = std::stoi(piece);
	setobjpose_srv.request.p.position.x = 0.17;
	setobjpose_srv.request.p.position.y = 0.27;
	setobjpose_srv.request.p.position.z = high1;
	setobjpose_srv.request.p.orientation.x = 0.0;
	setobjpose_srv.request.p.orientation.y = 0.0;
	setobjpose_srv.request.p.orientation.z = 0.0;
	setobjpose_srv.request.p.orientation.w = 1.0;
	setobjpose_client.call(setobjpose_srv);

	robot_pose(0.20, 0.27, high2, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the attach service
	ros::service::waitForService("/chesslab_setup/attachobs2robot");
	ros::ServiceClient attachobs2robot_client = node.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
	chesslab_setup::attachobs2robot attachobs2robot_srv;

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose(0.20, 0.27, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(0.20, 0.175, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	return true;

}



bool servtest4(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;
	std::string chessbox;
	float x_cell;
	float y_cell;

	float arucoxposes[] = { posx201, posx202, posx203, posx204, posx205, posx206, posx207, posx208, posx209, posx210, posx211, posx212, posx213, posx214, posx215, posx216, posx301, posx302, posx303, posx304, posx305, posx306, posx307, posx308, posx309, posx310, posx311, posx312, posx313, posx314, posx315, posx316};

	float arucoyposes[] = { posy201, posy202, posy203, posy204, posy205, posy206, posy207, posy208, posy209, posy210, posy211, posy212, posy213, posy214, posy215, posy216, posy301, posy302, posy303, posy304, posy305, posy306, posy307, posy308, posy309, posy310, posy311, posy312, posy313, posy314, posy315, posy316};

	float* poses[] = {arucoxposes, arucoyposes};
	
	ROS_INFO_STREAM(poses);
	std::string piece;
	std::cout << "Please select the piece to be picked: ";
	std::getline (std::cin,piece);

	if (piece == "201") {
		y_cell = -poses[0][0];
		x_cell = poses[1][0]+0.37;
	}
	if (piece == "202") {
		y_cell = -poses[0][1];
		x_cell = poses[1][1]+0.37;
	}
	if (piece == "203") {
		y_cell = -poses[0][2];
		x_cell = poses[1][2]+0.37;
	}
	if (piece == "204") {
		y_cell = -poses[0][3];
		x_cell = poses[1][3]+0.37;
	}
	if (piece == "205") {
		y_cell = -poses[0][4];
		x_cell = poses[1][4]+0.37;
	}
	if (piece == "206") {
		y_cell = -poses[0][5];
		x_cell = poses[1][5]+0.37;
	}
	if (piece == "207") {
		y_cell = -poses[0][6];
		x_cell = poses[1][6]+0.37;
	}
	if (piece == "208") {
		y_cell = -poses[0][7];
		x_cell = poses[1][7]+0.37;
	}
	if (piece == "209") {
		y_cell = -poses[0][8];
		x_cell = poses[1][8]+0.37;
	}
	if (piece == "210") {
		y_cell = -poses[0][9];
		x_cell = poses[1][9]+0.37;
	}
	if (piece == "211") {
		y_cell = -poses[0][10];
		x_cell = poses[1][10]+0.37;
	}
	if (piece == "212") {
		y_cell = -poses[0][11];
		x_cell = poses[1][11]+0.37;
	}
	if (piece == "213") {
		y_cell = -poses[0][12];
		x_cell = poses[1][12]+0.37;
	}
	if (piece == "214") {
		y_cell = -poses[0][13];
		x_cell = poses[1][13]+0.37;
	}
	if (piece == "215") {
		y_cell = -poses[0][14];
		x_cell = poses[1][14]+0.37;
	}
	if (piece == "216") {
		y_cell = -poses[0][15];
		x_cell = poses[1][15]+0.37;
	}
	if (piece == "301") {
		y_cell = -poses[0][16];
		x_cell = poses[1][16]+0.37;
	}
	if (piece == "302") {
		y_cell = -poses[0][17];
		x_cell = poses[1][17]+0.37;
	}
	if (piece == "303") {
		y_cell = -poses[0][18];
		x_cell = poses[1][18]+0.37;
	}
	if (piece == "304") {
		y_cell = -poses[0][19];
		x_cell = poses[1][19]+0.37;
	}
	if (piece == "305") {
		y_cell = -poses[0][20];
		x_cell = poses[1][20]+0.37;
	}
	if (piece == "307") {
		y_cell = -poses[0][21];
		x_cell = poses[1][21]+0.37;
	}
	if (piece == "308") {
		y_cell = -poses[0][22];
		x_cell = poses[1][22]+0.37;
	}
	if (piece == "309") {
		y_cell = -poses[0][23];
		x_cell = poses[1][23]+0.37;
	}
	if (piece == "310") {
		y_cell = -poses[0][24];
		x_cell = poses[1][24]+0.37;
	}
	if (piece == "311") {
		y_cell = -poses[0][25];
		x_cell = poses[1][25]+0.37;
	}
	if (piece == "312") {
		y_cell = -poses[0][26];
		x_cell = poses[1][26]+0.37;
	}
	if (piece == "313") {
		y_cell = -poses[0][27];
		x_cell = poses[1][27]+0.37;
	}
	if (piece == "314") {
		y_cell = -poses[0][28];
		x_cell = poses[1][28]+0.37;
	}
	if (piece == "315") {
		y_cell = -poses[0][29];
		x_cell = poses[1][29]+0.37;
	}
	if (piece == "316") {
		y_cell = -poses[0][30];
		x_cell = poses[1][30]+0.37;
	}

	ROS_INFO("Moving the robot to the pre-grasp configuration of the desired cell");

	float high2;
	float high3;
	if ((piece=="215")||(piece=="216")||(piece=="315")||(piece=="316")){
		high2=0.275;
		high3=0.36;
	}
	else if ((piece=="209")||(piece=="210")||(piece=="211")||(piece=="212")||(piece=="213")||(piece=="214")||(piece=="309")||(piece=="310")||(piece=="311")||(piece=="312")||(piece=="313")||(piece=="314"))
	{
		high2=0.245;
		high3=0.34;
	}
	else 
	{
		high2=0.235;
		high3=0.32;
	}

	robot_pose(x_cell, y_cell, 0.35, 0.707, -0.707, 0.0, 0.0, 0.85);
	robot_pose(x_cell, y_cell, 0.35-(0.35-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.85);
	robot_pose(x_cell, y_cell, high2, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the attach service
	ros::service::waitForService("/chesslab_setup/attachobs2robot");
	ros::ServiceClient attachobs2robot_client = node.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
	chesslab_setup::attachobs2robot attachobs2robot_srv;

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose(x_cell, y_cell, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	return true;

}



bool servtest5(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;

	float xposes[] = { posyA1, posyA2, posyA3, posyA4, posyA5, posyA6, posyA7, posyA8, posyB1, posyB2, posyB3, posyB4, posyB5, posyB6, posyB7, posyB8, posyC1, posyC2, posyC3, posyC4, posyC5, posyC6, posyC7, posyC8, posyD1, posyD2, posyD3, posyD4, posyD5, posyD6, posyD7, posyD8, posyE1, posyE2, posyE3, posyE4, posyE5, posyE6, posyE7, posyE8, posyF1, posyF2, posyF3, posyF4, posyF5, posyF6, posyF7, posyF8, posyG1, posyG2, posyG3, posyG4, posyG5, posyG6, posyG7, posyG8, posyH1, posyH2, posyH3, posyH4, posyH5, posyH6, posyH7, posyH8};

	float yposes[] = { posxA1, posxA2, posxA3, posxA4, posxA5, posxA6, posxA7, posxA8, posxB1, posxB2, posxB3, posxB4, posxB5, posxB6, posxB7, posxB8, posxC1, posxC2, posxC3, posxC4, posxC5, posxC6, posxC7, posxC8, posxD1, posxD2, posxD3, posxD4, posxD5, posxD6, posxD7, posxD8, posxE1, posxE2, posxE3, posxE4, posxE5, posxE6, posxE7, posxE8, posxF1, posxF2, posxF3, posxF4, posxF5, posxF6, posxF7, posxF8, posxG1, posxG2, posxG3, posxG4, posxG5, posxG6, posxG7, posxG8, posxH1, posxH2, posxH3, posxH4, posxH5, posxH6, posxH7, posxH8};



	float* poses[] = {xposes, yposes};

	std::string piece;
	std::string chessbox;
	float x_cell;
	float y_cell;


	std::cout << "Please select the piece to be placed: ";
	std::getline (std::cin,piece);

	std::cout << "Please select the cell where move the robot: ";
	std::getline (std::cin,chessbox);

	if (chessbox == "A1") {
		y_cell = -poses[0][0];
		x_cell = poses[1][0]+0.37;
	}
	if (chessbox == "A2") {
		y_cell = -poses[0][1];
		x_cell = poses[1][1]+0.37;
	}
	if (chessbox == "A3") {
		y_cell = -poses[0][2];
		x_cell = poses[1][2]+0.37;
	}
	if (chessbox == "A4") {
		y_cell = -poses[0][3];
		x_cell = poses[1][3]+0.37;
	}
	if (chessbox == "A5") {
		y_cell = -poses[0][4];
		x_cell = poses[1][4]+0.37;
	}
	if (chessbox == "A6") {
		y_cell = -poses[0][5];
		x_cell = poses[1][5]+0.37;
	}
	if (chessbox == "A7") {
		y_cell = -poses[0][6];
		x_cell = poses[1][6]+0.37;
	}
	if (chessbox == "A8") {
		y_cell = -poses[0][7];
		x_cell = poses[1][7]+0.37;
	}

	if (chessbox == "B1") {
		y_cell = -poses[0][8];
		x_cell = poses[1][8]+0.37;
	}
	if (chessbox == "B2") {
		y_cell = -poses[0][9];
		x_cell = poses[1][9]+0.37;
	}
	if (chessbox == "B3") {
		y_cell = -poses[0][10];
		x_cell = poses[1][10]+0.37;
	}
	if (chessbox == "B4") {
		y_cell = -poses[0][11];
		x_cell = poses[1][11]+0.37;
	}
	if (chessbox == "B5") {
		y_cell = -poses[0][12];
		x_cell = poses[1][12]+0.37;
	}
	if (chessbox == "B6") {
		y_cell = -poses[0][13];
		x_cell = poses[1][13]+0.37;
	}
	if (chessbox == "B7") {
		y_cell = -poses[0][14];
		x_cell = poses[1][14]+0.37;
	}
	if (chessbox == "B8") {
		y_cell = -poses[0][15];
		x_cell = poses[1][15]+0.37;
	}

	if (chessbox == "C1") {
		y_cell = -poses[0][16];
		x_cell = poses[1][16]+0.37;
	}
	if (chessbox == "C2") {
		y_cell = -poses[0][17];
		x_cell = poses[1][17]+0.37;
	}
	if (chessbox == "C3") {
		y_cell = -poses[0][18];
		x_cell = poses[1][18]+0.37;
	}
	if (chessbox == "C4") {
		y_cell = -poses[0][19];
		x_cell = poses[1][19]+0.37;
	}
	if (chessbox == "C5") {
		y_cell = -poses[0][20];
		x_cell = poses[1][20]+0.37;
	}
	if (chessbox == "C6") {
		y_cell = -poses[0][21];
		x_cell = poses[1][21]+0.37;
	}
	if (chessbox == "C7") {
		y_cell = -poses[0][22];
		x_cell = poses[1][22]+0.37;
	}
	if (chessbox == "C8") {
		y_cell = -poses[0][23];
		x_cell = poses[1][23]+0.37;
	}

	if (chessbox == "D1") {
		y_cell = -poses[0][24];
		x_cell = poses[1][24]+0.37;
	}
	if (chessbox == "D2") {
		y_cell = -poses[0][25];
		x_cell = poses[1][25]+0.37;
	}
	if (chessbox == "D3") {
		y_cell = -poses[0][26];
		x_cell = poses[1][26]+0.37;
	}
	if (chessbox == "D4") {
		y_cell = -poses[0][27];
		x_cell = poses[1][27]+0.37;
	}
	if (chessbox == "D5") {
		y_cell = -poses[0][28];
		x_cell = poses[1][28]+0.37;
	}
	if (chessbox == "D6") {
		y_cell = -poses[0][29];
		x_cell = poses[1][29]+0.37;
	}
	if (chessbox == "D7") {
		y_cell = -poses[0][30];
		x_cell = poses[1][30]+0.37;
	}
	if (chessbox == "D8") {
		y_cell = -poses[0][31];
		x_cell = poses[1][31]+0.37;
	}

	if (chessbox == "E1") {
		y_cell = -poses[0][32];
		x_cell = poses[1][32]+0.37;
	}
	if (chessbox == "E2") {
		y_cell = -poses[0][33];
		x_cell = poses[1][33]+0.37;
	}
	if (chessbox == "E3") {
		y_cell = -poses[0][34];
		x_cell = poses[1][34]+0.37;
	}
	if (chessbox == "E4") {
		y_cell = -poses[0][35];
		x_cell = poses[1][35]+0.37;
	}
	if (chessbox == "E5") {
		y_cell = -poses[0][36];
		x_cell = poses[1][36]+0.37;
	}
	if (chessbox == "E6") {
		y_cell = -poses[0][37];
		x_cell = poses[1][37]+0.37;
	}
	if (chessbox == "E7") {
		y_cell = -poses[0][38];
		x_cell = poses[1][38]+0.37;
	}
	if (chessbox == "E8") {
		y_cell = -poses[0][39];
		x_cell = poses[1][39]+0.37;
	}

	if (chessbox == "F1") {
		y_cell = -poses[0][40];
		x_cell = poses[1][40]+0.37;
	}
	if (chessbox == "F2") {
		y_cell = -poses[0][41];
		x_cell = poses[1][41]+0.37;
	}
	if (chessbox == "F3") {
		y_cell = -poses[0][42];
		x_cell = poses[1][42]+0.37;
	}
	if (chessbox == "F4") {
		y_cell = -poses[0][43];
		x_cell = poses[1][43]+0.37;
	}
	if (chessbox == "F5") {
		y_cell = -poses[0][44];
		x_cell = poses[1][44]+0.37;
	}
	if (chessbox == "F6") {
		y_cell = -poses[0][45];
		x_cell = poses[1][45]+0.37;
	}
	if (chessbox == "F7") {
		y_cell = -poses[0][46];
		x_cell = poses[1][46]+0.37;
	}
	if (chessbox == "F8") {
		y_cell = -poses[0][47];
		x_cell = poses[1][47]+0.37;
	}

	if (chessbox == "G1") {
		y_cell = -poses[0][48];
		x_cell = poses[1][48]+0.37;
	}
	if (chessbox == "G2") {
		y_cell = -poses[0][49];
		x_cell = poses[1][49]+0.37;
	}
	if (chessbox == "G3") {
		y_cell = -poses[0][50];
		x_cell = poses[1][50]+0.37;
	}
	if (chessbox == "G4") {
		y_cell = -poses[0][51];
		x_cell = poses[1][51]+0.37;
	}
	if (chessbox == "G5") {
		y_cell = -poses[0][52];
		x_cell = poses[1][52]+0.37;
	}
	if (chessbox == "G6") {
		y_cell = -poses[0][53];
		x_cell = poses[1][53]+0.37;
	}
	if (chessbox == "G7") {
		y_cell = -poses[0][54];
		x_cell = poses[1][54]+0.37;
	}
	if (chessbox == "G8") {
		y_cell = -poses[0][55];
		x_cell = poses[1][55]+0.37;
	}

	if (chessbox == "H1") {
		y_cell = -poses[0][56];
		x_cell = poses[1][56]+0.37;
	}
	if (chessbox == "H2") {
		y_cell = -poses[0][57];
		x_cell = poses[1][57]+0.37;
	}
	if (chessbox == "H3") {
		y_cell = -poses[0][58];
		x_cell = poses[1][58]+0.37;
	}
	if (chessbox == "H4") {
		y_cell = -poses[0][59];
		x_cell = poses[1][59]+0.37;
	}
	if (chessbox == "H5") {
		y_cell = -poses[0][60];
		x_cell = poses[1][60]+0.37;
	}
	if (chessbox == "H6") {
		y_cell = -poses[0][61];
		x_cell = poses[1][61]+0.37;
	}
	if (chessbox == "H7") {
		y_cell = -poses[0][62];
		x_cell = poses[1][62]+0.37;
	}
	if (chessbox == "H8") {
		y_cell = -poses[0][63];
		x_cell = poses[1][63]+0.37;
	}

	ROS_INFO_STREAM("Moving the robot to the pre-grasp configuration of the desired cell" << x_cell << "===" << y_cell);

	float high2;
	float high3;
	if ((piece=="215")||(piece=="216")||(piece=="315")||(piece=="316")){
		high2=0.275;
		high3=0.36;
	}
	else if ((piece=="209")||(piece=="210")||(piece=="211")||(piece=="212")||(piece=="213")||(piece=="214")||(piece=="309")||(piece=="310")||(piece=="311")||(piece=="312")||(piece=="313")||(piece=="314"))
	{
		high2=0.245;
		high3=0.34;
	}
	else 
	{
		high2=0.235;
		high3=0.32;
	}

	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cell, y_cell, high2+0.001, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the dettach service
	ros::service::waitForService("/chesslab_setup/dettachobs");
	ros::ServiceClient dettachobs_client = node.serviceClient<chesslab_setup::dettachobs>("/chesslab_setup/dettachobs");
	chesslab_setup::dettachobs dettachobs_srv;

	dettachobs_srv.request.robotName =  "team_A";
	dettachobs_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	dettachobs_client.call(dettachobs_srv);

	robot_pose(x_cell, y_cell, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.0);

	return true;
}

bool servtest6(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;
	std::string loc;
	float x_cell;
	float y_cell;
	std::string piece;

	std::cout << "Please select the piece to be moved: ";
	std::getline (std::cin,piece);

	float high1;
	float high2;
	float high3;
	if ((piece=="215")||(piece=="216")||(piece=="315")||(piece=="316"))
	{
		high1=0.0;
		high2=0.235;
		high3=0.36;
	}
	else if ((piece=="209")||(piece=="210")||(piece=="211")||(piece=="212")||(piece=="213")||(piece=="214")||(piece=="309")||(piece=="310")||(piece=="311")||(piece=="312")||(piece=="313")||(piece=="314"))
	{
		high1=-0.01;
		high2=0.215;
		high3=0.34;
	}
	else 
	{
		high1=-0.02;
		high2=0.195;
		high3=0.32;
	}

	std::cout << "Do you want to move the piece to the safety area ( write safe) or killed one ( write kill )? ";
	std::getline (std::cin,loc);

	if (loc=="safe")
	{
		x_cell=0.20;
		y_cell=0.27;
		ROS_INFO("Moving the piece to the safety area");
	}
	else if (loc=="kill")
	{
		x_cell=0.20;
		y_cell=-0.27;
		ROS_INFO("Moving the piece to the killed area");
	}

	//Move piece
	ros::service::waitForService("/chesslab_setup/setobjpose");
	ros::ServiceClient setobjpose_client = node.serviceClient<chesslab_setup::setobjpose>("/chesslab_setup/setobjpose");
	chesslab_setup::setobjpose setobjpose_srv;

	setobjpose_srv.request.objid = std::stoi(piece);
	setobjpose_srv.request.p.position.x = 0.17;
	setobjpose_srv.request.p.position.y = 0.27;
	setobjpose_srv.request.p.position.z = high1;
	setobjpose_srv.request.p.orientation.x = 0.0;
	setobjpose_srv.request.p.orientation.y = 0.0;
	setobjpose_srv.request.p.orientation.z = 0.0;
	setobjpose_srv.request.p.orientation.w = 1.0;
	setobjpose_client.call(setobjpose_srv);

	robot_pose(0.20, 0.27, high2, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the attach service
	ros::service::waitForService("/chesslab_setup/attachobs2robot");
	ros::ServiceClient attachobs2robot_client = node.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
	chesslab_setup::attachobs2robot attachobs2robot_srv;

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose(0.20, 0.27, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(0.20, 0.175, high3, 0.707, -0.707, 0.0, 0.0, 0.59);

	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cell, y_cell, high2, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the dettach service
	ros::service::waitForService("/chesslab_setup/dettachobs");
	ros::ServiceClient dettachobs_client = node.serviceClient<chesslab_setup::dettachobs>("/chesslab_setup/dettachobs");
	chesslab_setup::dettachobs dettachobs_srv;

	dettachobs_srv.request.robotName =  "team_A";
	dettachobs_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	dettachobs_client.call(dettachobs_srv);

	robot_pose(x_cell, y_cell, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.0);


	return true;
}


bool servtest7(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;
	std::string chessbox;
	float x_cell;
	float y_cell;

	float arucoxposes[] = { posx201, posx202, posx203, posx204, posx205, posx206, posx207, posx208, posx209, posx210, posx211, posx212, posx213, posx214, posx215, posx216, posx301, posx302, posx303, posx304, posx305, posx306, posx307, posx308, posx309, posx310, posx311, posx312, posx313, posx314, posx315, posx316};

	float arucoyposes[] = { posy201, posy202, posy203, posy204, posy205, posy206, posy207, posy208, posy209, posy210, posy211, posy212, posy213, posy214, posy215, posy216, posy301, posy302, posy303, posy304, posy305, posy306, posy307, posy308, posy309, posy310, posy311, posy312, posy313, posy314, posy315, posy316};

	float* aposes[] = {arucoxposes, arucoyposes};

	float xposes[] = { posyA1, posyA2, posyA3, posyA4, posyA5, posyA6, posyA7, posyA8, posyB1, posyB2, posyB3, posyB4, posyB5, posyB6, posyB7, posyB8, posyC1, posyC2, posyC3, posyC4, posyC5, posyC6, posyC7, posyC8, posyD1, posyD2, posyD3, posyD4, posyD5, posyD6, posyD7, posyD8, posyE1, posyE2, posyE3, posyE4, posyE5, posyE6, posyE7, posyE8, posyF1, posyF2, posyF3, posyF4, posyF5, posyF6, posyF7, posyF8, posyG1, posyG2, posyG3, posyG4, posyG5, posyG6, posyG7, posyG8, posyH1, posyH2, posyH3, posyH4, posyH5, posyH6, posyH7, posyH8};

	float yposes[] = { posxA1, posxA2, posxA3, posxA4, posxA5, posxA6, posxA7, posxA8, posxB1, posxB2, posxB3, posxB4, posxB5, posxB6, posxB7, posxB8, posxC1, posxC2, posxC3, posxC4, posxC5, posxC6, posxC7, posxC8, posxD1, posxD2, posxD3, posxD4, posxD5, posxD6, posxD7, posxD8, posxE1, posxE2, posxE3, posxE4, posxE5, posxE6, posxE7, posxE8, posxF1, posxF2, posxF3, posxF4, posxF5, posxF6, posxF7, posxF8, posxG1, posxG2, posxG3, posxG4, posxG5, posxG6, posxG7, posxG8, posxH1, posxH2, posxH3, posxH4, posxH5, posxH6, posxH7, posxH8};

	float* poses[] = {xposes, yposes};

	float x_cellB;
	float y_cellB;
	
	
	std::string piece;
	std::cout << "Please select the piece to be picked: ";
	std::getline (std::cin,piece);



	if (piece == "201") {
		y_cell = -aposes[0][0];
		x_cell = aposes[1][0]+0.37;
	}
	if (piece == "202") {
		y_cell = -aposes[0][1];
		x_cell = aposes[1][1]+0.37;
	}
	if (piece == "203") {
		y_cell = -aposes[0][2];
		x_cell = aposes[1][2]+0.37;
	}
	if (piece == "204") {
		y_cell = -aposes[0][3];
		x_cell = aposes[1][3]+0.37;
	}
	if (piece == "205") {
		y_cell = -aposes[0][4];
		x_cell = aposes[1][4]+0.37;
	}
	if (piece == "206") {
		y_cell = -aposes[0][5];
		x_cell = aposes[1][5]+0.37;
	}
	if (piece == "207") {
		y_cell = -aposes[0][6];
		x_cell = aposes[1][6]+0.37;
	}
	if (piece == "208") {
		y_cell = -aposes[0][7];
		x_cell = aposes[1][7]+0.37;
	}
	if (piece == "209") {
		y_cell = -aposes[0][8];
		x_cell = aposes[1][8]+0.37;
	}
	if (piece == "210") {
		y_cell = -aposes[0][9];
		x_cell = aposes[1][9]+0.37;
	}
	if (piece == "211") {
		y_cell = -aposes[0][10];
		x_cell = aposes[1][10]+0.37;
	}
	if (piece == "212") {
		y_cell = -aposes[0][11];
		x_cell = aposes[1][11]+0.37;
	}
	if (piece == "213") {
		y_cell = -aposes[0][12];
		x_cell = aposes[1][12]+0.37;
	}
	if (piece == "214") {
		y_cell = -aposes[0][13];
		x_cell = aposes[1][13]+0.37;
	}
	if (piece == "215") {
		y_cell = -aposes[0][14];
		x_cell = aposes[1][14]+0.37;
	}
	if (piece == "216") {
		y_cell = -aposes[0][15];
		x_cell = aposes[1][15]+0.37;
	}
	if (piece == "301") {
		y_cell = -aposes[0][16];
		x_cell = aposes[1][16]+0.37;
	}
	if (piece == "302") {
		y_cell = -aposes[0][17];
		x_cell = aposes[1][17]+0.37;
	}
	if (piece == "303") {
		y_cell = -aposes[0][18];
		x_cell = aposes[1][18]+0.37;
	}
	if (piece == "304") {
		y_cell = -aposes[0][19];
		x_cell = aposes[1][19]+0.37;
	}
	if (piece == "305") {
		y_cell = -aposes[0][20];
		x_cell = aposes[1][20]+0.37;
	}
	if (piece == "307") {
		y_cell = -aposes[0][21];
		x_cell = aposes[1][21]+0.37;
	}
	if (piece == "308") {
		y_cell = -aposes[0][22];
		x_cell = aposes[1][22]+0.37;
	}
	if (piece == "309") {
		y_cell = -aposes[0][23];
		x_cell = aposes[1][23]+0.37;
	}
	if (piece == "310") {
		y_cell = -aposes[0][24];
		x_cell = aposes[1][24]+0.37;
	}
	if (piece == "311") {
		y_cell = -aposes[0][25];
		x_cell = aposes[1][25]+0.37;
	}
	if (piece == "312") {
		y_cell = -aposes[0][26];
		x_cell = aposes[1][26]+0.37;
	}
	if (piece == "313") {
		y_cell = -aposes[0][27];
		x_cell = aposes[1][27]+0.37;
	}
	if (piece == "314") {
		y_cell = -aposes[0][28];
		x_cell = aposes[1][28]+0.37;
	}
	if (piece == "315") {
		y_cell = -aposes[0][29];
		x_cell = aposes[1][29]+0.37;
	}
	if (piece == "316") {
		y_cell = -aposes[0][30];
		x_cell = aposes[1][30]+0.37;
	}


	std::cout << "Please select the original cell of the piece: ";
	std::getline (std::cin,chessbox);


	if (chessbox == "A1") {
		y_cellB = -poses[0][0];
		x_cellB = poses[1][0]+0.37;
	}
	if (chessbox == "A2") {
		y_cellB = -poses[0][1];
		x_cellB = poses[1][1]+0.37;
	}
	if (chessbox == "A3") {
		y_cellB = -poses[0][2];
		x_cellB = poses[1][2]+0.37;
	}
	if (chessbox == "A4") {
		y_cellB = -poses[0][3];
		x_cellB = poses[1][3]+0.37;
	}
	if (chessbox == "A5") {
		y_cellB = -poses[0][4];
		x_cellB = poses[1][4]+0.37;
	}
	if (chessbox == "A6") {
		y_cellB = -poses[0][5];
		x_cellB = poses[1][5]+0.37;
	}
	if (chessbox == "A7") {
		y_cellB = -poses[0][6];
		x_cellB = poses[1][6]+0.37;
	}
	if (chessbox == "A8") {
		y_cellB = -poses[0][7];
		x_cellB = poses[1][7]+0.37;
	}
	if (chessbox == "B1") {
		y_cellB = -poses[0][8];
		x_cellB = poses[1][8]+0.37;
	}
	if (chessbox == "B2") {
		y_cellB = -poses[0][9];
		x_cellB = poses[1][9]+0.37;
	}
	if (chessbox == "B3") {
		y_cellB = -poses[0][10];
		x_cellB = poses[1][10]+0.37;
	}
	if (chessbox == "B4") {
		y_cellB = -poses[0][11];
		x_cellB = poses[1][11]+0.37;
	}
	if (chessbox == "B5") {
		y_cellB = -poses[0][12];
		x_cellB = poses[1][12]+0.37;
	}
	if (chessbox == "B6") {
		y_cellB = -poses[0][13];
		x_cellB = poses[1][13]+0.37;
	}
	if (chessbox == "B7") {
		y_cellB = -poses[0][14];
		x_cellB = poses[1][14]+0.37;
	}
	if (chessbox == "B8") {
		y_cellB = -poses[0][15];
		x_cellB = poses[1][15]+0.37;
	}
	if (chessbox == "C1") {
		y_cellB = -poses[0][16];
		x_cellB = poses[1][16]+0.37;
	}
	if (chessbox == "C2") {
		y_cellB = -poses[0][17];
		x_cellB = poses[1][17]+0.37;
	}
	if (chessbox == "C3") {
		y_cellB = -poses[0][18];
		x_cellB = poses[1][18]+0.37;
	}
	if (chessbox == "C4") {
		y_cellB = -poses[0][19];
		x_cellB = poses[1][19]+0.37;
	}
	if (chessbox == "C5") {
		y_cellB = -poses[0][20];
		x_cellB = poses[1][20]+0.37;
	}
	if (chessbox == "C6") {
		y_cellB = -poses[0][21];
		x_cellB = poses[1][21]+0.37;
	}
	if (chessbox == "C7") {
		y_cellB = -poses[0][22];
		x_cellB = poses[1][22]+0.37;
	}
	if (chessbox == "C8") {
		y_cellB = -poses[0][23];
		x_cellB = poses[1][23]+0.37;
	}
	if (chessbox == "D1") {
		y_cellB = -poses[0][24];
		x_cellB = poses[1][24]+0.37;
	}
	if (chessbox == "D2") {
		y_cellB = -poses[0][25];
		x_cellB = poses[1][25]+0.37;
	}
	if (chessbox == "D3") {
		y_cellB = -poses[0][26];
		x_cellB = poses[1][26]+0.37;
	}
	if (chessbox == "D4") {
		y_cellB = -poses[0][27];
		x_cellB = poses[1][27]+0.37;
	}
	if (chessbox == "D5") {
		y_cellB = -poses[0][28];
		x_cellB = poses[1][28]+0.37;
	}
	if (chessbox == "D6") {
		y_cellB = -poses[0][29];
		x_cellB = poses[1][29]+0.37;
	}
	if (chessbox == "D7") {
		y_cellB = -poses[0][30];
		x_cellB = poses[1][30]+0.37;
	}
	if (chessbox == "D8") {
		y_cellB = -poses[0][31];
		x_cellB = poses[1][31]+0.37;
	}
	if (chessbox == "E1") {
		y_cellB = -poses[0][32];
		x_cellB = poses[1][32]+0.37;
	}
	if (chessbox == "E2") {
		y_cellB = -poses[0][33];
		x_cellB = poses[1][33]+0.37;
	}
	if (chessbox == "E3") {
		y_cellB = -poses[0][34];
		x_cellB = poses[1][34]+0.37;
	}
	if (chessbox == "E4") {
		y_cellB = -poses[0][35];
		x_cellB = poses[1][35]+0.37;
	}
	if (chessbox == "E5") {
		y_cellB = -poses[0][36];
		x_cellB = poses[1][36]+0.37;
	}
	if (chessbox == "E6") {
		y_cellB = -poses[0][37];
		x_cellB = poses[1][37]+0.37;
	}
	if (chessbox == "E7") {
		y_cellB = -poses[0][38];
		x_cellB = poses[1][38]+0.37;
	}
	if (chessbox == "E8") {
		y_cellB = -poses[0][39];
		x_cellB = poses[1][39]+0.37;
	}
	if (chessbox == "F1") {
		y_cellB = -poses[0][40];
		x_cellB = poses[1][40]+0.37;
	}
	if (chessbox == "F2") {
		y_cellB = -poses[0][41];
		x_cellB = poses[1][41]+0.37;
	}
	if (chessbox == "F3") {
		y_cellB = -poses[0][42];
		x_cellB = poses[1][42]+0.37;
	}
	if (chessbox == "F4") {
		y_cellB = -poses[0][43];
		x_cellB = poses[1][43]+0.37;
	}
	if (chessbox == "F5") {
		y_cellB = -poses[0][44];
		x_cellB = poses[1][44]+0.37;
	}
	if (chessbox == "F6") {
		y_cellB = -poses[0][45];
		x_cellB = poses[1][45]+0.37;
	}
	if (chessbox == "F7") {
		y_cellB = -poses[0][46];
		x_cellB = poses[1][46]+0.37;
	}
	if (chessbox == "F8") {
		y_cellB = -poses[0][47];
		x_cellB = poses[1][47]+0.37;
	}
	if (chessbox == "G1") {
		y_cellB = -poses[0][48];
		x_cellB = poses[1][48]+0.37;
	}
	if (chessbox == "G2") {
		y_cellB = -poses[0][49];
		x_cellB = poses[1][49]+0.37;
	}
	if (chessbox == "G3") {
		y_cellB = -poses[0][50];
		x_cellB = poses[1][50]+0.37;
	}
	if (chessbox == "G4") {
		y_cellB = -poses[0][51];
		x_cellB = poses[1][51]+0.37;
	}
	if (chessbox == "G5") {
		y_cellB = -poses[0][52];
		x_cellB = poses[1][52]+0.37;
	}
	if (chessbox == "G6") {
		y_cellB = -poses[0][53];
		x_cellB = poses[1][53]+0.37;
	}
	if (chessbox == "G7") {
		y_cellB = -poses[0][54];
		x_cellB = poses[1][54]+0.37;
	}
	if (chessbox == "G8") {
		y_cellB = -poses[0][55];
		x_cellB = poses[1][55]+0.37;
	}
	if (chessbox == "H1") {
		y_cellB = -poses[0][56];
		x_cellB = poses[1][56]+0.37;
	}
	if (chessbox == "H2") {
		y_cellB = -poses[0][57];
		x_cellB = poses[1][57]+0.37;
	}
	if (chessbox == "H3") {
		y_cellB = -poses[0][58];
		x_cellB = poses[1][58]+0.37;
	}
	if (chessbox == "H4") {
		y_cellB = -poses[0][59];
		x_cellB = poses[1][59]+0.37;
	}
	if (chessbox == "H5") {
		y_cellB = -poses[0][60];
		x_cellB = poses[1][60]+0.37;
	}
	if (chessbox == "H6") {
		y_cellB = -poses[0][61];
		x_cellB = poses[1][61]+0.37;
	}
	if (chessbox == "H7") {
		y_cellB = -poses[0][62];
		x_cellB = poses[1][62]+0.37;
	}
	if (chessbox == "H8") {
		y_cellB = -poses[0][63];
		x_cellB = poses[1][63]+0.37;
	}


	float high2;
	float high3;
	if ((piece=="215")||(piece=="216")||(piece=="315")||(piece=="316"))
	{
		high2=0.275;
		high3=0.36;
	}
	else if ((piece=="209")||(piece=="210")||(piece=="211")||(piece=="212")||(piece=="213")||(piece=="214")||(piece=="309")||(piece=="310")||(piece=="311")||(piece=="312")||(piece=="313")||(piece=="314"))
	{
		high2=0.245;
		high3=0.34;
	}
	else 
	{
		high2=0.235;
		high3=0.32;
	}

	robot_pose(x_cell, y_cell, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cell, y_cell, 0.35-(0.35-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cell, y_cell, high2, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the attach service
	ros::service::waitForService("/chesslab_setup/attachobs2robot");
	ros::ServiceClient attachobs2robot_client = node.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
	chesslab_setup::attachobs2robot attachobs2robot_srv;

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose(x_cell, y_cell, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.59);

	robot_pose(x_cellB, y_cellB, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cellB, y_cellB, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cellB, y_cellB, high2+0.00001, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the dettach service
	ros::service::waitForService("/chesslab_setup/dettachobs");
	ros::ServiceClient dettachobs_client = node.serviceClient<chesslab_setup::dettachobs>("/chesslab_setup/dettachobs");
	chesslab_setup::dettachobs dettachobs_srv;

	dettachobs_srv.request.robotName =  "team_A";
	dettachobs_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	dettachobs_client.call(dettachobs_srv);

	robot_pose(x_cellB, y_cellB, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cellB, y_cellB, high3, 0.707, -0.707, 0.0, 0.0, 0.0);


	return true;
}


bool servtest8(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;
	std::string chessbox;
	float x_cell;
	float y_cell;

	float arucoxposes[] = { posx201, posx202, posx203, posx204, posx205, posx206, posx207, posx208, posx209, posx210, posx211, posx212, posx213, posx214, posx215, posx216, posx301, posx302, posx303, posx304, posx305, posx306, posx307, posx308, posx309, posx310, posx311, posx312, posx313, posx314, posx315, posx316};

	float arucoyposes[] = { posy201, posy202, posy203, posy204, posy205, posy206, posy207, posy208, posy209, posy210, posy211, posy212, posy213, posy214, posy215, posy216, posy301, posy302, posy303, posy304, posy305, posy306, posy307, posy308, posy309, posy310, posy311, posy312, posy313, posy314, posy315, posy316};

	float* poses[] = {arucoxposes, arucoyposes};
	
	ROS_INFO_STREAM(poses);
	std::string piece;
	std::cout << "Please select the piece to be killed: ";
	std::getline (std::cin,piece);

	if (piece == "201") {
		y_cell = -poses[0][0];
		x_cell = poses[1][0]+0.37;
	}
	if (piece == "202") {
		y_cell = -poses[0][1];
		x_cell = poses[1][1]+0.37;
	}
	if (piece == "203") {
		y_cell = -poses[0][2];
		x_cell = poses[1][2]+0.37;
	}
	if (piece == "204") {
		y_cell = -poses[0][3];
		x_cell = poses[1][3]+0.37;
	}
	if (piece == "205") {
		y_cell = -poses[0][4];
		x_cell = poses[1][4]+0.37;
	}
	if (piece == "206") {
		y_cell = -poses[0][5];
		x_cell = poses[1][5]+0.37;
	}
	if (piece == "207") {
		y_cell = -poses[0][6];
		x_cell = poses[1][6]+0.37;
	}
	if (piece == "208") {
		y_cell = -poses[0][7];
		x_cell = poses[1][7]+0.37;
	}
	if (piece == "209") {
		y_cell = -poses[0][8];
		x_cell = poses[1][8]+0.37;
	}
	if (piece == "210") {
		y_cell = -poses[0][9];
		x_cell = poses[1][9]+0.37;
	}
	if (piece == "211") {
		y_cell = -poses[0][10];
		x_cell = poses[1][10]+0.37;
	}
	if (piece == "212") {
		y_cell = -poses[0][11];
		x_cell = poses[1][11]+0.37;
	}
	if (piece == "213") {
		y_cell = -poses[0][12];
		x_cell = poses[1][12]+0.37;
	}
	if (piece == "214") {
		y_cell = -poses[0][13];
		x_cell = poses[1][13]+0.37;
	}
	if (piece == "215") {
		y_cell = -poses[0][14];
		x_cell = poses[1][14]+0.37;
	}
	if (piece == "216") {
		y_cell = -poses[0][15];
		x_cell = poses[1][15]+0.37;
	}
	if (piece == "301") {
		y_cell = -poses[0][16];
		x_cell = poses[1][16]+0.37;
	}
	if (piece == "302") {
		y_cell = -poses[0][17];
		x_cell = poses[1][17]+0.37;
	}
	if (piece == "303") {
		y_cell = -poses[0][18];
		x_cell = poses[1][18]+0.37;
	}
	if (piece == "304") {
		y_cell = -poses[0][19];
		x_cell = poses[1][19]+0.37;
	}
	if (piece == "305") {
		y_cell = -poses[0][20];
		x_cell = poses[1][20]+0.37;
	}
	if (piece == "307") {
		y_cell = -poses[0][21];
		x_cell = poses[1][21]+0.37;
	}
	if (piece == "308") {
		y_cell = -poses[0][22];
		x_cell = poses[1][22]+0.37;
	}
	if (piece == "309") {
		y_cell = -poses[0][23];
		x_cell = poses[1][23]+0.37;
	}
	if (piece == "310") {
		y_cell = -poses[0][24];
		x_cell = poses[1][24]+0.37;
	}
	if (piece == "311") {
		y_cell = -poses[0][25];
		x_cell = poses[1][25]+0.37;
	}
	if (piece == "312") {
		y_cell = -poses[0][26];
		x_cell = poses[1][26]+0.37;
	}
	if (piece == "313") {
		y_cell = -poses[0][27];
		x_cell = poses[1][27]+0.37;
	}
	if (piece == "314") {
		y_cell = -poses[0][28];
		x_cell = poses[1][28]+0.37;
	}
	if (piece == "315") {
		y_cell = -poses[0][29];
		x_cell = poses[1][29]+0.37;
	}
	if (piece == "316") {
		y_cell = -poses[0][30];
		x_cell = poses[1][30]+0.37;
	}


	float high1;
	float high2;
	float high3;
	if ((piece=="215")||(piece=="216")||(piece=="315")||(piece=="316"))
	{
		high1=0.235;
		high2=0.275;
		high3=0.36;
	}
	else if ((piece=="209")||(piece=="210")||(piece=="211")||(piece=="212")||(piece=="213")||(piece=="214")||(piece=="309")||(piece=="310")||(piece=="311")||(piece=="312")||(piece=="313")||(piece=="314"))
	{
		high1=0.215;
		high2=0.245;
		high3=0.34;
	}
	else 
	{
		high1=0.195;
		high2=0.235;
		high3=0.32;
	}

	robot_pose(x_cell, y_cell, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cell, y_cell, 0.35-(0.35-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_cell, y_cell, high2, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the attach service
	ros::service::waitForService("/chesslab_setup/attachobs2robot");
	ros::ServiceClient attachobs2robot_client = node.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
	chesslab_setup::attachobs2robot attachobs2robot_srv;

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose(x_cell, y_cell, high3-(high3-high2)/2, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_cell, y_cell, high3, 0.707, -0.707, 0.0, 0.0, 0.59);

	robot_pose(0.20, -0.27, high3, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(0.20, -0.27, high3-(high3-high1)/2, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(0.20, -0.27, high1, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the dettach service
	ros::service::waitForService("/chesslab_setup/dettachobs");
	ros::ServiceClient dettachobs_client = node.serviceClient<chesslab_setup::dettachobs>("/chesslab_setup/dettachobs");
	chesslab_setup::dettachobs dettachobs_srv;

	dettachobs_srv.request.robotName =  "team_A";
	dettachobs_srv.request.objarucoid =  std::stoi(piece);
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	dettachobs_client.call(dettachobs_srv);

	robot_pose(0.20, -0.27, high3-(high3-high1)/2, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(0.20, -0.27, high3, 0.707, -0.707, 0.0, 0.0, 0.0);

	return true;

}

bool servtest9(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node;
	std::string castle;

	std::cout << "Do you want to make the castle operation with the right ( write right ) or left ( left ) tower? ";
	std::getline (std::cin,castle);

	robot_pose((posy216+0.37), -posx216, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose((posy216+0.37), -posx216, 0.30, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose((posy216+0.37), -posx216, 0.275, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the attach service
	ros::service::waitForService("/chesslab_setup/attachobs2robot");
	ros::ServiceClient attachobs2robot_client = node.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
	chesslab_setup::attachobs2robot attachobs2robot_srv;

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  216;
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose((posy216+0.37), -posx216, 0.30, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose((posy216+0.37), -posx216, 0.35, 0.707, -0.707, 0.0, 0.0, 0.59);

	float y_king;
	float x_king;
	float tower;
	float y_tower;
	float x_tower;
	float y_tower2;
	float x_tower2;
	if (castle=="right")
	{
	y_king=-posyB8;
	x_king=posxB8+0.37;
	tower=210;
	y_tower=-posx210;
	x_tower=posy210+0.37;
	y_tower2=-posyC8;
	x_tower2=posxC8+0.37;
	}
	else if (castle=="left")
	{
	y_king=-posyG8;
	x_king=posxG8+0.37;
	tower=209;
	y_tower=-posx209;
	x_tower=posy209+0.37;
	y_tower2=-posyF8;
	x_tower2=posxF8+0.37;
	}

	

	robot_pose(x_king, y_king, 0.35, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_king, y_king, 0.30, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_king, y_king, 0.276, 0.707, -0.707, 0.0, 0.0, 0.59);

	//call the dettach service
	ros::service::waitForService("/chesslab_setup/dettachobs");
	ros::ServiceClient dettachobs_client = node.serviceClient<chesslab_setup::dettachobs>("/chesslab_setup/dettachobs");
	chesslab_setup::dettachobs dettachobs_srv;

	dettachobs_srv.request.robotName =  "team_A";
	dettachobs_srv.request.objarucoid =  216;
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	dettachobs_client.call(dettachobs_srv);

	robot_pose(x_king, y_king, 0.30, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_king, y_king, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);

	robot_pose(x_tower, y_tower, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_tower, y_tower, 0.30, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_tower, y_tower, 0.245, 0.707, -0.707, 0.0, 0.0, 0.59);

	attachobs2robot_srv.request.robotName =  "team_A";
	attachobs2robot_srv.request.objarucoid =  tower;
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	attachobs2robot_client.call(attachobs2robot_srv);

	robot_pose(x_tower, y_tower, 0.30, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_tower, y_tower, 0.35, 0.707, -0.707, 0.0, 0.0, 0.59);

	robot_pose(x_tower2, y_tower2, 0.35, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_tower2, y_tower2, 0.30, 0.707, -0.707, 0.0, 0.0, 0.59);
	robot_pose(x_tower2, y_tower2, 0.246, 0.707, -0.707, 0.0, 0.0, 0.59);

	dettachobs_srv.request.robotName =  "team_A";
	dettachobs_srv.request.objarucoid =  tower;
	//The following delay is necessary to let the robot reach the final configuration before calling the attach (the exact value not analyzed)
	ros::Duration(1).sleep();
	dettachobs_client.call(dettachobs_srv);

	robot_pose(x_tower2, y_tower2, 0.30, 0.707, -0.707, 0.0, 0.0, 0.0);
	robot_pose(x_tower2, y_tower2, 0.35, 0.707, -0.707, 0.0, 0.0, 0.0);

	return true;

}


bool servtest10(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp){

	ros::NodeHandle node; 
	ros::service::waitForService("/chesslab_setup/resetscene");
	ros::ServiceClient resetscene_client1 = node.serviceClient<std_srvs::Empty>("/chesslab_setup/resetscene");
	std_srvs::Empty resetscene_srv;
	resetscene_client1.call(resetscene_srv);

	return true;

}

int main(int argc, char** argv){
	ros::init(argc, argv, "my_tf2_listener");	
	ros::NodeHandle node; 


	ros::ServiceServer server1 =
	node.advertiseService("test_1",&servtest1);

	ros::ServiceServer server2 =
	node.advertiseService("test_2",&servtest2);

	ros::ServiceServer server3 =
	node.advertiseService("test_3",&servtest3);

	ros::ServiceServer server4 =
	node.advertiseService("test_4",&servtest4);

	ros::ServiceServer server5 =
	node.advertiseService("test_5",&servtest5);

	ros::ServiceServer server6 =
	node.advertiseService("test_6",&servtest6);

	ros::ServiceServer server7 =
	node.advertiseService("pick_place",&servtest7);

	ros::ServiceServer server8 =
	node.advertiseService("kill_piece",&servtest8);

	ros::ServiceServer server9 =
	node.advertiseService("castle",&servtest9);

	ros::ServiceServer server10 =
	node.advertiseService("reset",&servtest10);

	//Set the initial scene
	ros::service::waitForService("/chesslab_setup/resetscene");
	ros::ServiceClient resetscene_client = node.serviceClient<std_srvs::Empty>("/chesslab_setup/resetscene");
	std_srvs::Empty resetscene_srv;
	resetscene_client.call(resetscene_srv);

	tf2_ros::Buffer tfBuffer;
	tf2_ros::TransformListener tfListener(tfBuffer);
	ros::Rate rate(30.0);
	

	while (node.ok()){

		geometry_msgs::TransformStamped transformStamped;

		try{
			transformStamped = tfBuffer.lookupTransform("A1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA1 = transformStamped.transform.translation.x;
		posyA1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA2 = transformStamped.transform.translation.x;
		posyA2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA3 = transformStamped.transform.translation.x;
		posyA3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA4 = transformStamped.transform.translation.x;
		posyA4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA5 = transformStamped.transform.translation.x;
		posyA5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA6 = transformStamped.transform.translation.x;
		posyA6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA7 = transformStamped.transform.translation.x;
		posyA7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("A8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxA8 = transformStamped.transform.translation.x;
		posyA8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB1 = transformStamped.transform.translation.x;
		posyB1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB2 = transformStamped.transform.translation.x;
		posyB2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB3 = transformStamped.transform.translation.x;
		posyB3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB4 = transformStamped.transform.translation.x;
		posyB4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB5 = transformStamped.transform.translation.x;
		posyB5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB6 = transformStamped.transform.translation.x;
		posyB6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB7 = transformStamped.transform.translation.x;
		posyB7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("B8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxB8 = transformStamped.transform.translation.x;
		posyB8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC1 = transformStamped.transform.translation.x;
		posyC1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC2 = transformStamped.transform.translation.x;
		posyC2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC3 = transformStamped.transform.translation.x;
		posyC3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC4 = transformStamped.transform.translation.x;
		posyC4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC5 = transformStamped.transform.translation.x;
		posyC5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC6 = transformStamped.transform.translation.x;
		posyC6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC7 = transformStamped.transform.translation.x;
		posyC7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("C8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxC8 = transformStamped.transform.translation.x;
		posyC8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD1 = transformStamped.transform.translation.x;
		posyD1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD2 = transformStamped.transform.translation.x;
		posyD2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD3 = transformStamped.transform.translation.x;
		posyD3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD4 = transformStamped.transform.translation.x;
		posyD4 = transformStamped.transform.translation.y;


		try{
			transformStamped = tfBuffer.lookupTransform("D5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD5 = transformStamped.transform.translation.x;
		posyD5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD6 = transformStamped.transform.translation.x;
		posyD6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD7 = transformStamped.transform.translation.x;
		posyD7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("D8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxD8 = transformStamped.transform.translation.x;
		posyD8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE1 = transformStamped.transform.translation.x;
		posyE1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE2 = transformStamped.transform.translation.x;
		posyE2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE3 = transformStamped.transform.translation.x;
		posyE3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE4 = transformStamped.transform.translation.x;
		posyE4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE5 = transformStamped.transform.translation.x;
		posyE5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE6 = transformStamped.transform.translation.x;
		posyE6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE7 = transformStamped.transform.translation.x;
		posyE7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("E8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxE8 = transformStamped.transform.translation.x;
		posyE8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF1 = transformStamped.transform.translation.x;
		posyF1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF2 = transformStamped.transform.translation.x;
		posyF2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF3 = transformStamped.transform.translation.x;
		posyF3 = transformStamped.transform.translation.y;


		try{
			transformStamped = tfBuffer.lookupTransform("F4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF4 = transformStamped.transform.translation.x;
		posyF4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF5 = transformStamped.transform.translation.x;
		posyF5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF6 = transformStamped.transform.translation.x;
		posyF6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF7 = transformStamped.transform.translation.x;
		posyF7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("F8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxF8 = transformStamped.transform.translation.x;
		posyF8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG1 = transformStamped.transform.translation.x;
		posyG1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG2 = transformStamped.transform.translation.x;
		posyG2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG3 = transformStamped.transform.translation.x;
		posyG3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG4 = transformStamped.transform.translation.x;
		posyG4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG5 = transformStamped.transform.translation.x;
		posyG5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG6 = transformStamped.transform.translation.x;
		posyG6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG7 = transformStamped.transform.translation.x;
		posyG7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("G8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxG8 = transformStamped.transform.translation.x;
		posyG8 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H1_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH1 = transformStamped.transform.translation.x;
		posyH1 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H2_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH2 = transformStamped.transform.translation.x;
		posyH2 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H3_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH3 = transformStamped.transform.translation.x;
		posyH3 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H4_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH4 = transformStamped.transform.translation.x;
		posyH4 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H5_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH5 = transformStamped.transform.translation.x;
		posyH5 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H6_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH6 = transformStamped.transform.translation.x;
		posyH6 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H7_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH7 = transformStamped.transform.translation.x;
		posyH7 = transformStamped.transform.translation.y;

		try{
			transformStamped = tfBuffer.lookupTransform("H8_CELL","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posxH8 = transformStamped.transform.translation.x;
		posyH8 = transformStamped.transform.translation.y;

		//End of Cell listeners

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_201","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx201 = transformStamped.transform.translation.x;
		posy201 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_202","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx202 = transformStamped.transform.translation.x;
		posy202 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_203","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx203 = transformStamped.transform.translation.x;
		posy203 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_204","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx204 = transformStamped.transform.translation.x;
		posy204 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_205","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}

		posx205 = transformStamped.transform.translation.x;
		posy205 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_206","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx206 = transformStamped.transform.translation.x;
		posy206 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_207","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx207 = transformStamped.transform.translation.x;
		posy207 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_208","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx208 = transformStamped.transform.translation.x;
		posy208 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_209","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx209 = transformStamped.transform.translation.x;
		posy209 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_210","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx210 = transformStamped.transform.translation.x;
		posy210 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_211","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx211 = transformStamped.transform.translation.x;
		posy211 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_212","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx212 = transformStamped.transform.translation.x;
		posy212 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_213","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx213 = transformStamped.transform.translation.x;
		posy213 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_214","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx214 = transformStamped.transform.translation.x;
		posy214 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_215","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx215 = transformStamped.transform.translation.x;
		posy215 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_216","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx216 = transformStamped.transform.translation.x;
		posy216 = transformStamped.transform.translation.z;

		//End of aruco 20x listeners

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_301","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx301 = transformStamped.transform.translation.x;
		posy301 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_302","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx302 = transformStamped.transform.translation.x;
		posy302 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_303","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx303 = transformStamped.transform.translation.x;
		posy303 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_304","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx304 = transformStamped.transform.translation.x;
		posy304 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_305","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx305 = transformStamped.transform.translation.x;
		posy305 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_306","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx306 = transformStamped.transform.translation.x;
		posy306 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_307","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx307 = transformStamped.transform.translation.x;
		posy307 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_308","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx308 = transformStamped.transform.translation.x;
		posy308 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_309","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx309 = transformStamped.transform.translation.x;
		posy309 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_310","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx310 = transformStamped.transform.translation.x;
		posy310 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_311","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx311 = transformStamped.transform.translation.x;
		posy311 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_312","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx312 = transformStamped.transform.translation.x;
		posy312 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_313","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx313 = transformStamped.transform.translation.x;
		posy313 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_314","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx314 = transformStamped.transform.translation.x;
		posy314 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_315","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx315 = transformStamped.transform.translation.x;
		posy315 = transformStamped.transform.translation.z;

		try{
			transformStamped = tfBuffer.lookupTransform("aruco_316","world",ros::Time(0));
		}
		catch (tf2::TransformException &ex) {
			ros::Duration(1.0).sleep();
			continue;
		}
		posx316 = transformStamped.transform.translation.x;
		posy316 = transformStamped.transform.translation.z;

		ros::spinOnce();
		rate.sleep();
	}
	return 0;
};




